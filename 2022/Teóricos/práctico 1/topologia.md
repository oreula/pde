## Práctico de Topología

#### Ejercicio 1.

Encuentre, para el caso de la topología de la linea real, una intersección infinita de 
abiertos que no es abierta.

#### Ejercicio 2.

a) Pruebe que un espacio métrico posee una topología *inducida* por su 
métrica en forma similar a $R$ en el ejemplo anterior.

b) Vea que $d(x,y)=1$ si $x\neq y$, es una distancia. ¿Qué topología nos introduce dicha distancia?

#### Ejercicio 3.

1. Sea $(X,d)$ un espacio vectorial métrico, pruebe que: 

    a. $C^1_x = \{x'| d(x,x') \leq 1 \}$ es cerrado y es un entorno de
$x$. 

    b) $N^{ϵ}_x =\{x' | d(x,x') < ϵ \}$, $ϵ >0$  es también un entorno
de $x$. 

    c) $Int(N^{ϵ}_x) = N^{ϵ}_x$

    d) $Cl(N^{ϵ}_x) = \{x' | d(x,x') \leq ϵ\} $

    e) $\partial N^{ϵ}_x = \{x' | d(x,x') = ϵ \}$.

2. Sea $(X,\cal{T})$ un espacio topológico y $A$ un subconjunto de $X$.
Pruebe que: 

    a) $A$ es abierto si y solo si cada $x \in A$ tiene un entorno 
contenido en $A$. 

    b) $A$ es cerrado si y solo si cada $x$ en $A^c$
(o sea no perteneciente a $A$) tiene un entorno que no intersecta a $A$.

3. Sea $(X,\cal{T})$ un espacio topológico, sea $A \in X$ y $x \in X$.
Pruebe que: 

    a) $x \in Int(A)$ si y solo si $x$ tiene un entorno contenido en $A$.

    b) $x \in Cl(A)$ si y solo si todo entorno de $x$ intersecta $A$. 

    c) $x \in \partial A$ si y solo si todo entorno de $x$ contiene puntos
en $A$ y puntos en $A^c$.

#### Ejercicio 4.

Repase la prueba del siguiente teorema:

*El mapa $ϕ:X → Y$ es continuo si y solo si se cumple que:
dado cualquier punto $x ∈ X$ y cualquier entorno $M$ de $ϕ(x)$,
existe un entorno $N$ de $x$ tal que $ϕ(N) ⊂ M$.*

#### Ejercicio 5.

Sea $ϕ : X → Y$ y $ψ : Y → Z$ mapas continuos,
pruebe que $ψ ∘ ϕ : X → Z$ también es continuo.
(Composición de mapas preserva continuidad.)

#### Ejercicio 6.

Pruebe que los conjuntos compactos en la línea real son los
cerrados y acotados.

#### Ejercicio 7.

a. Pruebe que si un espacio es Hausdorff entonces si una sucesión tiene un punto límite este es único.

b. Sea $X$ compacto, $Y$ Hausdorff y $ϕ: X → Y$ continuo. Pruebe que las imágenes de conjuntos cerrados son cerradas. 

c. Encuentre un contra-ejemplo si $Y$ no es Hausdorff. 




