\documentclass[10pt]{amsart}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\title{ODE Fundamental Theorem using Euler Method}
\author{Oscar Reula}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
%\section{}
%\subsection{}

We want to make a proof of the fundamental theorem of Ordinary Differential Equations using Euler's integration method. That is, mimicking the actual integration methods rather than using an iteration method like Picard's.

We write the equation as,
\[
\frac{d x}{dt} = f(x)
\]
where $x \in R^n$. 

%\begin{theorem}{Teorema Fundamental para ODEs}

Fundamental Theorem for ODE's:

Assume that there is a point $x_c$ and a positive radius $R$ such that $f(x)$ exists and is Lifshitz on $B_{2R}(x_c)$ (given by $|x-x_c|<2R$).
Then given any point $x_0 \in B_{R}(x_c)$ there is a finite time interval $[0,T]$ for which a unique solution to the above system exists and satisfies $x(0)=x_0$.
%\end{theorem}

Let $F:= \max_{x \in B_{2R}(x_c)}{|f(x)|}$, and fix $T < R/F$ so that the solution, if exist, can not leave the ball. 
We also define $L$ so that $|f(x) - f(y)| < L |x-y|$ the Lipschitz constant. 

We divide the integration interval $[0,T]$ into $N_0$ steps of size $h_0= T/N_{0}$, that is, $t_n = n*h$ and compute 
the finite sequence, (for clarity we use $h$ instead of $h_{0}$)
\[
y_h^{n+1} = y_h^n + h f(y_h^n)
\]
We then approximate the solution by the linear interpolation joining these points, calling it $x_h(t)$. 

\[
x_h(t) = y_h^n + \frac{(t - nh)}{h}(y_h^{n+1} - y_h^n) \;\;\;\;\; t \in [nh, (n+1)h]
\]

We repeat the procedure using more points, namely $N_i = 2^i N_0$, and correspondingly $h_i = h2^{-i}$.
Thus we get an infinite sequence of continuous approximations to the would be solution. Since the space of continuous functions, 
$C[0,T]$ is Banach (complete) under the norm $max$. If we can show that the above sequence is Cauchy we would find a unique limiting
function on that space and that would be the solution we are seeking. 

So we must proof now it is Cauchy in the $max$ norm. It is enough to compare two consecutive elements on the sequence and show
that we can make their difference as small as we want if they are far enough along the sequence.

So we consider now the $i$ and the $i+1$ elements of the sequences of continuous functions, and call $h_i = h$, so $h_{i+1}=h/2$. 
Consider any interval of the bigger sequence, say the one going from $nh$ to $(n+1)h$, in that interval the finer sequence would consist of two straight lines, while the initial one is just a single line.
We assert that the maximum distance among these lines must be at one of the three points where the lines break, $x_{h/2}(nh)$, $x_{h/2}((n+1/2)h)$, or $x_{h/2}((n+)h)$ (see lemma at the end).
We express them in terms of the first one, for the middle point we get,

\begin{eqnarray*}
&& ||x_h((n+1/2)h) - x_{h/2}((n+1/2)h)|| \\
				&=& ||x_h(nh) + \frac{h}{2} f(x_h(nh)) - [x_{h/2}(nh) + \frac{h}{2} f(x_{h/2}(nh)] || \\
				&=& ||x_h(nh) - x_{h/2}(nh) - \frac{h}{2} [f(x_{h/2}(nh))- f(x_h(nh))]|| \\
				&\leq& ||x_h(nh) - x_{h/2}(nh)|| + \frac{h}{2} ||f(x_{h/2}(nh))- f(x_h(nh))|| \\
				&\leq& |1+hL/2| ||x_h(nh) - x_{h/2}(nh)||
\end{eqnarray*}				

For the end point we get, 

\begin{eqnarray*}
&& ||x_h((n+1)h) - x_{h/2}((n+1)h)|| \\
				&=& ||x_h(nh) + h f(x_h(nh)) - [x_{h/2}(nh) + \frac{h}{2} f(x_{h/2}(nh)) + \frac{h}{2} f(x_{h/2}(nh) + \frac{h}{2} f(x_{h/2}(nh)))] || \\
				&=& ||x_h(nh) - x_{h/2}(nh) + h [f(x_{h}(nh)) - f(x_{h/2}(nh))] - \frac{h}{2} [f(x_{h/2}(nh) + \frac{h}{2} f(x_{h/2}(nh))) - f(x_{h/2}(nh)) || \\
				&\leq& ||x_h(nh) - x_{h/2}(nh)|| + h ||f(x_{h/2}(nh)) - f(x_h(nh))|| + \frac{h}{2} ||f(x_{h/2}(nh) + \frac{h}{2} f(x_{h/2}(nh))) - f(x_{h/2}(nh)) || \\
				&\leq& ||x_h(nh) - x_{h/2}(nh)|| + h ||f(x_{h/2}(nh)) - f(x_h(nh))|| + \frac{h}{2} L||\frac{h}{2} f(x_{h/2}(nh))|| \\
				&\leq& |1+hL/2| ||x_h(nh) - x_{h/2}(nh)|| + h^2 LF/4
\end{eqnarray*}	
%

So we can bound the approximations in the $[nh, (n+1)h]$ interval as,

\begin{eqnarray*}
max_{t\in[nh,(n+1)h]} ||x_h(t) - x_{h/2}(t)|| &\leq& |1+hL/2| ||x_h(nh) - x_{h/2}(nh)|| + h^2 LF/4 \\
                                                                &\leq& |1+hL/2|\; max_{t\in[(n-1)h,nh]} ||x_h(t) - x_{h/2}(t)|| + h^2 LF/4
\end{eqnarray*}

Iterating this from $n=0$ (where $x_h(0) = x_{h/2}(0)$) we get that

\begin{eqnarray*}
max_{t\in[0,T]} || x_h(t) - x_{h/2}(t) || &\leq& \sum_{j=0}^N (1+hL/2)^j h^2LF/4 \\
			&=& \frac{(1+hL/2)^{N+1} - 1}{(1+hL/2) -1} h^2 LF/4 \\
			&=& ((1 + \frac{TL}{2N})^{N+1}-1) hF/2 \\
			&\leq& (e^{TL}-1) hF/2
\end{eqnarray*}

Since $h=2^{-i}h_0 \to 0$, by going far enough in the sequence we can make this difference as small as we please. 
From the rate at which these diferences go to zero we conclude that the sequence is Cauchy. 
See the second Lemma at the end of this section.

Thus we have a unique limiting sequence that we call $x(t)$.

We want to check now that this limiting function satisfies the integral version of our equation,

\[
x(t) = x_0 + \int_0^t f(x(\tilde{t})) d\tilde t.
\]
%
Once we have this then we can derive it to obtain the differential equation.

Given $t \in [0,T]$ we choose $n_i$ to be the integer part of $2^i t / h_0$ then $n_i h_i \to t$ as $i \to \infty$.
By continuity $x(n_i h_i) \to x(t)$, furthermore, $x_{h_i}(n_i h_i) \to x(t)$.
%
But, 

\[
x_{h_i}(n_i h_i) = x_0 + \sum_{j=0}^{n_i-1} h_i f(x_{h_i}(j h_i)) ,
\]
Since $f$ is continuos the above sum converges to the Riemann integral. This completes the existence and uniqueness proof. 


\textbf{Lemma:}

\textsl{The maximum distance between corresponding points along two straight lines is attained at some of the extremes.}

\textbf{Proof:}

Consider two straight line curves connecting each two pairs of points: $x_1 + t(x_2 - x_1)$ and $x'_1 + t(x'_2 - x'_1)$.
The distance between them (for the same $t$) is, 

\[
|| x_1 + t(x_2 - x_1) - [x'_1 + t(x'_2 - x'_1)]|| = || (x_1 - x'_1) + t[(x_2 - x'_2)  - (x_1 - x'_1)]||.
\]
% 
That is the distance to the origin of a line segment, starting at $x_1 - x'_1$ and ending at $x_2 - x'_2$. When that distance is measured by a norm, then the maximum is at one of the extremes of the interval. To see this, start with a level surface of the norm that is so large that contains the line, then start shrinking it until touches the line segment for the first time, that point would be the point of maximal norm. 
But that point can not be a point not in the extremes, unless the whole line segment touches the level surface at once.
This is because the levels surfaces of a norm are convex surfaces, and so, if it touches in an interior point then all points must be at that surface. 

 
\textbf{Lemma:} 
If the difference among two consecutive members of a sequence goes to cero as $Cs^n$ with $s<1$ then the sequence is Cauchy.

We assume then that $e_n := ||x_n-x_{n+1}|| \leq C s^n$

Then we have,

$$
\begin{align}
||x_n-x_m|| &= ||x_n - x_{n+1} + x_{n+1} - x_{n+2} + \ldots + x_{m-1} - x_{m}|| \\
			&\leq ||x_n - x_{n+1}|| + ||x_{n+1} - x_{n+2}|| + \ldots + ||x_{m-1} - x_{m}|| \\
			&= \sum_{l=n}^m e_l \\
			&= C \sum_{l=n}^m s^l \\
			&= C s^n \sum_{l=0}^{m-n} s^l \\
			&= C s^n \frac{1-s^{m-n+1}}{1-s} \\
			&\leq C \frac{s^n}{1-s}
\end{align}
$$
Since $s<1$ the difference tends to zero as $n\to \infty$, regardless of the value of $m$. 



\end{document}  