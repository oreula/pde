\documentclass[11pt]{amsart}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\usepackage{physics}

%%%%
%%%% The next few commands set up the theorem type environments.
%%%% Here they are set up to be numbered section.number, but this can
%%%% be changed.
%%%%

\newtheorem{thm}{Theorem}[section]
\newtheorem{prop}[thm]{Proposition}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{cor}[thm]{Corollary}


\theoremstyle{definition}
\newtheorem{definition}[thm]{Definition}
\newtheorem{example}[thm]{Example}

%%
%% Again more of these can be added by uncommenting and editing the
%% following. 
%%

%\newtheorem{note}[thm]{Note}


%%% 
%%% The following gives remark type environments (which only differ
%%% from theorem type invironmants in the choices of fonts).  The
%%% numbering is still tied to the theorem counter.
%%% 


\theoremstyle{remark}
\newtheorem{remark}[thm]{Remark}

\newtheorem{exercise}[thm]{Exercise}
\newtheorem{problem}[thm]{Problem}
%%%
%%% The following, if uncommented, numbers equations within sections.
%%% 

\numberwithin{equation}{section}


%%%
%%% The following show how to make definition (also called macros or
%%% abbreviations).  For example to use get a bold face R for use to
%%% name the real numbers the command is \mathbf{R}.  To save typing we
%%% can abbreviate as

\newcommand{\R}{\mathbf{R}}  % The real numbers.

%%
%% The comment after the defintion is not required, but if you are
%% working with someone they will likely thank you for explaining your
%% definition.  
%%
%% Now add you own definitions:
%%

%%%
%%% Mathematical operators (things like sin and cos which are used as
%%% functions and have slightly different spacing when typeset than
%%% variables are defined as follows:
%%%

\DeclareMathOperator{\dist}{dist} % The distance.



%%
%% This is the end of the preamble.
%% 

\title{Sobolev Inequalities}
\author{Oscar Reula}
%\date{}                                           % Activate to display a given date or no date

\begin{document}

%\begin{abstract}
%We show a couple of Discrete Sobolev Inequalities in one dimension.
%\end{abstract}

\maketitle


%\section{Introduction}

We want to prove the clasical Sobolev's inequality which relates boundedness of functions and their derivatives 
to bounds on the integral of the squares of some derivatives of the solutions. 

Here we shall do in its simplest way, using Fourier series, that is in local regions which we can cover with pieces of torus. 
Starting from that one can extend the proof to unbounded regions as long as they satisfy weak cone properties. 
But here we stay with the simplest cases since that is all what we are going to use. 

\section{One dimensional case}

We shall study functions in a circle of length $2\pi$, $C$, the result can be extended to arbitrary circles by scaling. 

In that circle we know that smooth functions can be expressed by their Fourier coefficients. 

That is, for each $x \in C$, 

\begin{equation}
\label{eqn:Fourier}
f(x) = \frac{1}{(2\pi)^{1/2}}\sum_{n=0}^{\infty} C_n e^{inx}
\end{equation}

where,

$$ 
C_n = \frac{1}{(2\pi)^{1/2}}\int_0^{2\pi} f(x)e^{-inx} \; dx
$$

If we take a derivative we get, 

$$
\frac{df}{dx}(x) = \frac{1}{(2\pi)^{1/2}}\sum_{n=0}^{\infty} in C_n e^{inx}
$$

Therefore we can express a Sobolev norm as:


\begin{align*}
||f||^2_{H^m(C)} &= \int_0^{2\pi} [|f|^2 + |\frac{df}{dx}|^2 + \cdots + |\frac{d^mf}{dx^m}|^2]\; dx \\
                  &= \sum_{n=0}^{\infty} [|C_n|^2 + n^2|C_n|^2 + \cdots + |C_n|^2 n^{2m}] \\
                  &= \sum_{n=0}^{\infty} |C_n|^2[1 + n^2 + \cdots + n^{2m}]
\end{align*}


Now, since there exists constants $C_-$ and $C^+$ such that

$$
C_- (1+n^{2})^m \leq  1 + n^2 + \cdots + n^{2m} \leq C^+ (1+n^{2})^m  
$$
%
we see that we could define the Sobolev norm in the circle as,

$$
||f||^2_{H^m(C)} := \sum_{n=0}^{\infty} |C_n|^2(1+n^{2})^m.
$$

This definition is nicer because we now can generalize the Sobolev norms to real values (even negative values!):
$$
||f||^2_{H^{\alpha}(C)} := \sum_{n=0}^{\infty} |C_n|^2(1+n^{2})^{\alpha}, \;\;\; \alpha \in \R
$$

For our proof of the Sobolev lemma we shall use this definition. 

From the first equations \ref{eqn:Fourier} we have,

\begin{align*}
|f(x)| &= |\frac{1}{(2\pi)^{1/2}}\sum_{n=0}^{\infty} C_n e^{inx}| \\
      &\leq \frac{1}{(2\pi)^{1/2}}\sum_{n=0}^{\infty} |C_n e^{inx}| \\
      &\leq \frac{1}{(2\pi)^{1/2}}\sum_{n=0}^{\infty} |C_n| \\
      &\leq \frac{1}{(2\pi)^{1/2}}\sum_{n=0}^{\infty} |C_n|(1+n^{2})^{\frac{\alpha}{2}} \frac{1}{(1+n^{2})^{\frac{\alpha}{2}}} \\
      &\leq \frac{1}{(2\pi)^{1/2}} \sqrt{\sum_{n=0}^{\infty} |C_n|^2 (1+n^{2})^{\alpha}} \sqrt{\sum_{n=0}^{\infty}\frac{1}{(1+n^{2})^{\alpha}}} \\
\end{align*}

So, if $\alpha> \frac{1}{2}$ then

$$
\sum_{n=0}^{\infty}\frac{1}{(1+n^{2})^{\alpha}} < \infty
$$

and if $f \in H_{\alpha}(C)$ we have a bound on $|f(x)|$ and conclude that there exists a constant $K$ such that for all smooth functions,

$$
||f||_{C^0(C)} \leq K ||f||_{H^{\alpha}(C)}
$$

Since the smooth functions are dense in both $C^0(C)$, and $H^{\alpha}(C)$, taking Cauchy sequences of these functions and using completeness we
conclude that this inequality is valid for all functions in $H^{\alpha}(C)$. That is all functions in $H^{\alpha}(C)$ are continuous. 

Applying these bound to deritives of the functions we get the Sobolev Lemma:

\begin{lem} If $\alpha > m + \frac{1}{2}$ then 
      $$
        H^{\alpha}(C) \subset C^m(C)
      $$
\end{lem}

\section{More dimensions}

In more dimensions the proof in a n-torus $T^n$ is similar,
the only difference of relevance is that in more dimensions the sums are over a multi-index 
$l = (l_1,l_2, \ldots, l_n)$   and so we must find out when the sums

$$
\sum_l \frac{1}{(1+l\cdot l)^{\alpha}} 
$$

Are finite. And analogy with similar integral expressions of the form,

$$ 
\int_{\R^n} \frac{1}{(1+r^2)^{\alpha}} \; d^n\vec{x} = \int_0^{\infty} \int_{\Omega} \frac{1}{(1+r^2)^{\alpha}} r^{(n-1)}\;dr\; d\Omega 
$$

asserts that the value is: $\alpha = \frac{n}{2}$ so that the Lemma becomes: 

\begin{lem} If $\alpha > m + \frac{n}{2}$ then 
      $$
        H^{\alpha}(T^n) \subset C^m(T^n)
      $$
\end{lem}

\begin{exercise}
      Find an element on $C^m(T^n)$ which is not in $H^{\alpha}(T^n)$.
\end{exercise}

\end{document}