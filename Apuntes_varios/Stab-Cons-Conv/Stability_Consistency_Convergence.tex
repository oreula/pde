\documentclass[11pt]{amsart}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\usepackage{physics}

%%%%
%%%% The next few commands set up the theorem type environments.
%%%% Here they are set up to be numbered section.number, but this can
%%%% be changed.
%%%%

\newtheorem{thm}{Theorem}[section]
\newtheorem{prop}[thm]{Proposition}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{cor}[thm]{Corollary}


\theoremstyle{definition}
\newtheorem{definition}[thm]{Definition}
\newtheorem{example}[thm]{Example}

%%
%% Again more of these can be added by uncommenting and editing the
%% following. 
%%

%\newtheorem{note}[thm]{Note}


%%% 
%%% The following gives remark type environments (which only differ
%%% from theorem type invironmants in the choices of fonts).  The
%%% numbering is still tied to the theorem counter.
%%% 


\theoremstyle{remark}

\newtheorem{remark}[thm]{Remark}


%%%
%%% The following, if uncommented, numbers equations within sections.
%%% 

\numberwithin{equation}{section}


%%%
%%% The following show how to make definition (also called macros or
%%% abbreviations).  For example to use get a bold face R for use to
%%% name the real numbers the command is \mathbf{R}.  To save typing we
%%% can abbreviate as

\newcommand{\R}{\mathbf{R}}  % The real numbers.

%%
%% The comment after the defintion is not required, but if you are
%% working with someone they will likely thank you for explaining your
%% definition.  
%%
%% Now add you own definitions:
%%

%%%
%%% Mathematical operators (things like sin and cos which are used as
%%% functions and have slightly different spacing when typeset than
%%% variables are defined as follows:
%%%

\DeclareMathOperator{\dist}{dist} % The distance.



%%
%% This is the end of the preamble.
%% 

\title{Stability, Consistency and Convergence}
\author{Oscar Reula}
%\date{}                                           % Activate to display a given date or no date

\begin{document}

\begin{abstract}
We discuss a simple, yet important result of Lax, it says that if a method is stable and consisten, then is also
convergent. We present the concepts and prove the theorem via a simple example, the 1D Laplacian.
\end{abstract}

\maketitle


\section{The setting}

We are dealing with some problem in the continuum and an algorithm to approximate it which satisfies certain
conditions. We want to show that if we refine the approximation by going closer and closer to the continuum we
converge to the continuum solution to the problem. The theorem is rather general and in fact nothing of we just stated appears in the proof. 
So to give a feeling for it we shall discuss every concept with a simple example using finite differences. 
But this is by no means the only way to use this theorem, in fact we can use it for finite elements approximations too.

The example would be the following:


For the example we shall consider the Laplace equation in a 1D interval, a line segment of length $X$ with homogeneous boundary conditions:

\[
L(u) := \frac{d^2 u}{dx^2} = f \;\;\;\;\;\;\;\;\;\; u(0) = u(X) = 0,
\]

and discretize it uniformly, that is $x_i = i*dx$, $i=0\ldots N$, $dx = \frac{X}{N-1}$.
We consider functions en $u:[0,L] \to R$ which vanish at the boundaries, and define 
$v_i = u(x_i)$, $i = 1,N-1$
We also define $D_{\pm}$ as the operator acting on sequences $\{v_i\}$ as 
$(D_{\pm} v)_i := \pm \frac{v_{i\pm 1} - v_{i}}{dx}$, 
that is the forwards / backwards finite difference approximation to the derivative. 
For $i=0,N$ we set $v_i$ to zero and solve for the rest,  $i=1 \ldots N-1$ the system,

\[
L_N(v)_i := (D_+D_- v)_i = f_i \;\;\;\;\;\;\;\;\;\; \forall i=1,N-1
\]

\section{Stability}

Stability is a concept that is closely related to continuity of the solution with respect to the sources (and boundary values too). Roughly speaking it says that the continuity is uniform as the approximating sequence gets better and better.

Let us first discuss continuity in the continuum case:


Integrating $u L(u)$ on the segment we get,

\[
- \int_0^X u L(u) dx = \int_0^X - u \frac{d^2 u}{dx^2} dx = \int_0^X \frac{d u}{dx} \frac{d u}{dx} dx + [u\frac{d u}{dx}]_0^X  = \int_0^X \frac{d u}{dx} \frac{d u}{dx} dx 
\]

That is, 

\[
|| \frac{d u}{dx} ||^2_{L^2} \leq || f ||_{L^2} || u ||_{L^2}
\]

Using now Poincare inequality, 

\[
|| u ||_{L^2} < X || \frac{d u}{dx} ||_{L^2}
\]

we get, 

\[
|| \frac{d u}{dx} ||^2_{L^2} \leq X || f ||_{L^2} || \frac{d u}{dx} ||_{L^2}, 
\]

or

\[
|| u ||_{H^1_0}^2 := \frac{1}{X^2}|| u ||_{L^2}^2 +  || \frac{d u}{dx} ||_{L^2}^2 \leq 2 X^2 || f ||_{L^2}^2 = 2 X^2 || L(u) ||_{L^2}^2,
\]

That is, $L^{-1}$, the map which sends sources to solutions, ($L^{-1}(f) = u$), is continuous (bounded) between the spaces $L^2$ and $H^1_0$. 

\[
|| L^{-1} || := sup_{|| f ||_{L^2}=1} \{|| L^{-1}(f) ||_{H^1_0} \} \leq 2X^2
\]

We shall see now the corresponding bound for the discrete system:

\[
\sum_{i=1}^{N-1} - v_i (D_+D_- v)_i = \sum_{i=1}^{N-1} (D_- v)_i D_- v)_i = - \sum_{i=1}^{N-1} v_i f_i
\]
Which follows just from the fact that $D_+$, as a matrix, is the transpose of $D_-$. 

Thus, we have, 

\[
|| D_- v ||_{l^2}^2 \leq || v ||_{l^2} || f ||_{l^2}.
\]

Using the Discrete Poincaré inequality, 

\[
 || v ||^2_{l^2} \leq X^2 || D_- v ||^2_{l^2} := X^2 || v ||_{h^1_0}^2
 \]
 
 We get, 
 
 \[
|| L_N^{-1} || := sup_{|| f ||_{l^2}=1} \{|| L_N^{-1}(f) ||_{h^1_0} \} \leq 2X^2 
\]

Thus, we see that there is a  bound on the norm of $L^{-1}$ which is independent of $N$.

This is what we call stability of the convergence scheme.

\textbf{Definition:} A sequence of problems, $\{$ Given $f^N \in Y^N$, find the unique $v^N \in X_N$ such that 
$L_N(v^N) = f^N$ $\}$ is \textbf{stable} if there exists a constant $C$ such that the family of inverse maps: $L_N^{-1}: Y^N \to X^N$ is bounded independent of $N$,

\[
|| L_N^{-1} || := sup_{|| f^N ||_{Y^N}=1} \{|| L_N^{-1}(f^N) ||_{X^N} \} \leq C  \;\;\;\;\; \forall N
\]


\section{Consistency}

Consistency is the property that asserts that the set of above problems are approximating the continuum problem.
To do that we need to measure the deviation of $L_N(v^N) - f^N$ from $L(u)-f$ in some appropriate way. 
Since they are in different spaces we need to embed one into the other. The best way to proceed here is to map the continuum problem into the discrete one. So we define $u^N$ in the same way as we have restricted $f$ to get $f^N$.

In our example we just take $v_i = u(x_i)$, and $f_i = f(x_i)$.

\textbf{Definition:} A sequence of problems, $\{$ Given $f^N \in Y^N$, find the unique $v^N \in X^N$ such that 
$L_N(v^N) = f^N$ $\}$ is \textbf{consistent} with the problem $L(u) = f$ if  for each $N$ there is a restriction $R_N:X \to X^N$
such that  $|| L_N(R_N(u)) - R_N(Lu) ||_{Y^N} \to 0$ as $N \to \infty$ for all $u$ in $X$.

In our example, 

\[
R_N(Lu)_i = \frac{d^2 u}{dx^2}(x_i)
\]

While, calling $v_i = R_N(u)_i = u(x_i)$,

\begin{eqnarray*}
L_N(v)_i   &=& \frac{v_{i+1} + v_{i-1} -2 v_i}{dx^2} \\
		&=& \frac{u(x_{i+1}) + u(x_{i-1}) -2 u(x_i)}{dx^2} \\
		&=& \frac{\frac{d u}{dx}(x_i) + \frac{d^2 u}{dx^2}(x_i) \frac{dx^2}{2} + \frac{d^3 u}{dx^3}(x_i)  \frac{dx^3}{6}  + \frac{d^4 u}{dx^4}(x_i)  \frac{dx^4}{24} }{dx^2}\\
		&+& \frac{-\frac{d u}{dx}(x_i) + \frac{d^2 u}{dx^2}(x_i) \frac{dx^2}{2} - \frac{d^3 u}{dx^3}(x_i)  \frac{dx^3}{6}  + \frac{d^4 u}{dx^4}(x_i)  \frac{dx^4}{24} }{dx^2} + \ldots \\
		&=& \frac{d^2 u}{dx^2}(x_i) +  \frac{d^4 u}{dx^4}(\zeta_i) \frac{dx^2}{12} 
\end{eqnarray*}

Thus, for $C^4$ solutions we get consistency in the $l^2$ norm, 
\[
|| L_N(R_N(u)) - R_N(Lu) ||_{l^2} \to 0 \;\;\;\;\; N \to \infty.
\]

Showing that our scheme is consistent.

\section{Convergence}

Is the property that asserts that the solutions to our discrete family of problems converge to the solution to the continuum problem. 
Again, to compare, we need to embed one space into the others. We choose to embed the continuum space into the discrete ones. After having shown convergence there, one can do the reverse embeding and show convergence in the continuum, but for that one only needs properties of the embeding map, and nothing else. 
 

 \textbf{Definition:} A sequence of solutions to the problems, $\{$ Given $f^N \in Y^N$, find the unique $v^N \in X^N$ such that 
$L_N(v^N) = f^N$ $\}$ is \textbf{convergent} to the solution of the limiting problem $L(u) = f$, if 

\[
||R_N(u) - v^N ||_{X^N} \to 0 \;\;\;\;\; N \to \infty.
\]

This is of course the interesting property. But actually is a consequence of the other two properties:

\thm{Convergence}{
Given a sequence of problems $\{$ Given $f^N \in Y_N$, find the unique $v^N \in X^N$ such that 
$L_N(v^N) = f^N$$\}$ if they are stable and consistent with a limiting problem $L(u) = f$, then the sequence of solutions is convergent.

Proof:

\begin{eqnarray*}
|| R_N(u) - v^N ||_{X^N} &=& || L_N^{-1}(L_N(R_N(u) ) - L_N^{-1}(f^N) ||_{X^N} \\
				&=&  || L_N^{-1}(L_N(R_N(u)) - R_N(L(u))) ||_{X^N} \\
				&\leq& ||L_N^{-1}||\; ||L_N(R_N(u)) - R_N(L(u)) ||_{Y^N} \\
				&\leq& C \; ||L_N(R_N(u)) - R_N(L(u)) ||_{Y^N}\; \to \;0
\end{eqnarray*} 
 
 Where: in the first step we have used that $L_N(v^N) = f^N$; in the second that $R_N(L(u) - f) = 0$; 
 in the third the usual bound for operators, and in the last one stability and consistency. }
 
 
 
\end{document}