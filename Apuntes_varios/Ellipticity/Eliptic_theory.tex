\documentclass[11pt]{amsart}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\usepackage{physics}

%%%%
%%%% The next few commands set up the theorem type environments.
%%%% Here they are set up to be numbered section.number, but this can
%%%% be changed.
%%%%

\newtheorem{thm}{Theorem}[section]
\newtheorem{prop}[thm]{Proposition}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{cor}[thm]{Corollary}


%%
%% If some other type is need, say conjectures, then it is constructed
%% by editing and uncommenting the following.
%%

%\newtheorem{conj}[thm]{Conjecture} 


%%% 
%%% The following gives definition type environments (which only differ
%%% from theorem type invironmants in the choices of fonts).  The
%%% numbering is still tied to the theorem counter.
%%% 

\theoremstyle{definition}
\newtheorem{definition}[thm]{Definition}
\newtheorem{example}[thm]{Example}

%%
%% Again more of these can be added by uncommenting and editing the
%% following. 
%%

%\newtheorem{note}[thm]{Note}


%%% 
%%% The following gives remark type environments (which only differ
%%% from theorem type invironmants in the choices of fonts).  The
%%% numbering is still tied to the theorem counter.
%%% 


\theoremstyle{remark}

\newtheorem{remark}[thm]{Remark}


%%%
%%% The following, if uncommented, numbers equations within sections.
%%% 

\numberwithin{equation}{section}


%%%
%%% The following show how to make definition (also called macros or
%%% abbreviations).  For example to use get a bold face R for use to
%%% name the real numbers the command is \mathbf{R}.  To save typing we
%%% can abbreviate as

\newcommand{\R}{\mathbf{R}}  % The real numbers.

%%
%% The comment after the defintion is not required, but if you are
%% working with someone they will likely thank you for explaining your
%% definition.  
%%
%% Now add you own definitions:
%%

%%%
%%% Mathematical operators (things like sin and cos which are used as
%%% functions and have slightly different spacing when typeset than
%%% variables are defined as follows:
%%%

\DeclareMathOperator{\dist}{dist} % The distance.



%%
%% This is the end of the preamble.
%% 

\title{Elliptic Theory}
\author{Oscar Reula}
%\date{}                                           % Activate to display a given date or no date

\begin{document}

\begin{abstract}

Here we discuss the elliptic theory. Notes based in Métodos Matemáticos de la Física and Arnolds Notes.

\end{abstract}

\maketitle


\section{Introduction}

The more often treated elliptic equation is the Laplacian with Dirichlet boundary conditions,

 \[
 \Delta u := e^{ij}\partial_i \partial_j u = \rho \;\;\;\;\;\;\;\;\; \in \Omega, \;\;\;\;\;\;\;\;\; u|_{\partial \Omega} = f
 \]
 
 We know that this equation has a unique solution in a given Sobolev space provided the source and boundary terms are also in appropriate Sobolev spaces, and the boundary is smooth enough.
 
 We can generalize the problem to operators of the type:
 
 \[
 a^{ij}\partial_i \partial_j u + b^j \partial_j u + c u = \rho \;\;\;\;\;\;\;\;\; \in \Omega, \;\;\;\;\;\;\;\;\; u |_{\partial \Omega} = f
\]
These ones might or might not have solutions, or might have several solutions, and we want to characterize that as much as possible. Furthermore we can consider systems where the coefficients depend on $u$ and $\partial u$, and also other boundary conditions.

Furthermore we can consider system of equations. To treat all this in a consistent and abridged way we consider systems of first order equations:

\[
K^{A i}{}_{\alpha} \nabla_i u^{\alpha} = J^{A} \;\;\;\;\;\;\;\;\; \in \Omega, \;\;\;\;\;\;\;\;\; B^{\Gamma}{}_{\alpha} |_{\partial \Omega} = f^{\Gamma}
\]

Where indices run on the space of fields, of equations and of boundary conditions. The coefficients depend on the fields in the fiber bundle sense (point wise). 

We shall say the system is elliptic at a point $(p,u)$ if the rank of $K^{Ai}{}_{\alpha} n_i$ is maximal for any $n_i$, and that it is strongly elliptic if there exist positive quadratic forms $G_{AB}$, $G_{\alpha \beta}$ and $g^{ij}$ such that,

\[
G_{AB} K^{A i}{}_{\alpha} n_i u^{\alpha}  K^{B j}{}_{\beta} n_j u^{\beta} \geq g^{ij}n_i n_j G_{\alpha \beta} u^{\alpha} u^{\beta} 
\]

\section{Case Study, the Laplacian}

Let be $\Omega$ a compact region with smooth boundary, $\partial \Omega$, in $R^n$.

We shall look for solutions to the equations:

\begin{align}
\Delta u &= f \;\;\; \text{in} \; \Omega \nonumber \\ 
u|_{\partial \Omega} &= 0, \nonumber
\end{align}
%
and show the existence and uniqueness of weak solutions to them. 

One could consider the more general \textbf{Dirichlet} problem, 

\begin{align}
\Delta u &= f \;\;\; \text{in} \; \Omega \nonumber \\ 
u|_{\partial \Omega} &= g, \nonumber
\end{align}
%
for some function $g$ defined in $\partial \Omega$ but this can be studied a posteriori as follows:
One first extend this function to a smoother function on $\Omega$, that is find $\tilde g \in \Omega$ such that
$\tilde g|_{\partial \Omega} = g$ and then define $\tilde u = u - \tilde g$. Now we have an equivalent problem,

\begin{align}
\Delta \tilde u &= \tilde f := f - \Delta \tilde g \;\;\; \text{in} \; \Omega \nonumber \\ 
\tilde u |_{\partial \Omega} &= 0. \nonumber
\end{align}
%

We shall look for solutions in the space $H^1_0(\Omega)$. That is the Sobolev space with norm $H^1(\Omega)$, 
but completing with functions of compact support inside $\Omega$. This way one gets a space whose elements have no support (in the sense of distributions) on $\partial \Omega$. For this space we have Poincaré Lemma:

\begin{lem}[Poincaré]{There exists $c> 0$ such that,

$$
\int_{\Omega} |\nabla u|^2\; d\Omega > c \int_{\Omega} |u|^2\; d\Omega.
$$
}
\end{lem}

This Lemma tells us that,

$$
<u,v>:= \int_{\Omega} |\nabla u|^2\; d\Omega 
$$
is a scalar product. Furthermore, the corresponding norm is equivalent to the Sobolev norm,

$$
||u||_{H^1}^2 = \int_{\Omega} [|u|^2 + |\nabla u|^2]\; d\Omega.
$$
Indeed, we have, 

$$
\frac{c}{1+c} ||u||_{H^1}^2 < \; <u,u> \; < ||u||_{H^1}^2.
$$

Let us see what does means the Riesz Representation Theorem for this scalar product.
Let us consider an element $\sigma$ on the dual of $H^1_0(\Omega)$, that is a continuous linear functional from 
$H^1_0(\Omega) \to R$. 
The theorem tell us that there exists a unique $u \in H^1_0(\Omega)$ such that,

$$
<v,u> = \sigma(v) \;\;\;\; \forall v \in H^1_0(\Omega)
$$ 

We claim that this $u$ is a weak solution to our Homogeneous Dirichlet problem.
Indeed the equation above says, 

$$
\int_{\Omega} \nabla u \cdot \nabla v \; d\Omega = \sigma(v) \;\;\;\; \forall v \in H^1_0(\Omega),
$$ 
% 
which is the distributional version of the Laplacian equation.

Thus, if $u$ where smoother we could integrate by parts to get:

$$
\int_{\Omega} - v \nabla  \cdot \nabla u \; d\Omega + \int_{\partial \Omega} v \; \vec{n} \cdot \nabla u \; dS = \sigma(v) \;\;\;\; \forall v \in H^1_0(\Omega),
$$

Since $v \in H^1_0(\Omega)$ the surface integral vanishes and we get, 

$$
\int_{\Omega} - v \nabla  \cdot \nabla u \; d\Omega = \sigma(v) \;\;\;\; \forall v \in H^1_0(\Omega).
$$
%
In particular, for instance if 

$$
\sigma(v) = - \int_{\Omega} f v \; d\Omega \;\;\; f \in L^2(\Omega)
$$
%
Notice that in this case,
$$
|\sigma(v)| =  |\int_{\Omega} f v \; d\Omega | \leq ||f||_{L^2} \; ||v||_{H^1},
$$
so $\sigma$ is indeed a linear continuous functional.
Thus, we conclude (since $v$ is arbitrary inside $\Omega$, that 

$$
\Delta u = f \; \in \Omega.
$$

Notice that, taking $v=u$ we have, 

$$
\frac{c}{1+c} ||u||_{H^1}^2 < \; <u,u> \; = |\sigma(u)| < ||f||_{L^2} ||u||_{H^1},
$$
%
and so,

$$
 ||u||_{H^1} < \frac{1+c}{c}||f||_{L^2}.
$$

That is, \textbf{the problem is stable}, the solution depends continuously on the source.


\section{Generalizations}

We want to see when does there exists a solution to the problem,

$$
a(u,v) = b(v)
$$

Where $a(\cdot,\cdot)$ is a continuous bilinear map from $V\times V \to C$, that is, there exists $M>0$ such that, 

$$
|a(u,v)| \leq M ||u||_V ||v||_V.
$$

And $b(\cdot)$ is a continuos linear map from $V \to C$, that is an element of $V'$. 
In particular we have, that there exists $B$ such that,

$$
||b||_{V'} = \sup_{||v||_V = 1}\{|b(v)|\} = B\;\;\;\; \Rightarrow\;\;\; |b(v)| \leq B ||v||_V
$$


\subsection{Symmetric coercitive case}

This is the case we have just seen, we requiere:

1) Symmetric:
$$
a(u,v) = a(v,u)
$$

2) Coercitive, there exists $\gamma > 0$.
$$
a(u,u) > \gamma ||u||^2_V.
$$

Symmetry, continuity and coercitivity imply that $a(u,v)$ is a scalar product with norm equivalent to the norm of $V$. 
From there the rest is Riesz Representation Theorem. We get existence, uniqueness and stability.

A nice example is the Neumann problem: 

Find $u \in H^1(\Omega)$ solution to:

\begin{align}
L u := \nabla \cdot (a \nabla u) - cu &= f \;\;\; \text{in} \; \Omega \nonumber \\ 
a\vec{n} \cdot \nabla u|_{\partial \Omega} &= 0, \nonumber
\end{align}
%
with $0 < \underline{c} < c < \bar{c}$, and $0 < \underline{a} < a < \bar{a}$.

The bilinear form is:

$$
a(u,v) = \int_{\Omega} a \nabla u \cdot \nabla v  + c uv \; d\Omega 
$$

Integrating by parts we get, 
$$
\int_{\Omega} [a \nabla u \cdot \nabla v  + c uv] \; d\Omega = \int_{\Omega} [-\nabla \cdot (a \nabla u)   + c u] v\; d\Omega - \int_{\partial \Omega} [v \vec{n}\cdot (a \nabla u) \; dS =   \int_{\Omega} fv \; d\Omega.
$$

Taking $v$ with support inside $\Omega$ we conclude that $u$ satisfies the equation. Then taking $v$ with support in $\partial \Omega$ we get the boundary condition. 



\subsection{Coercitive}

In this case we consider a system which is not necessarily symmetric.

In that case, using Riesz representation theorem we can define a map from $V$ to $V$, that is an element of $\mathcal{L}(V,V)$, by the relation,

$$
a(w,v) := \left<A(w),v \right> \;\forall\; w,v \in V.
$$
%
We can conclude the following:

\begin{itemize}

	\item $A$ is continuous. 
	Indeed, taking 
	$$
	||A(w)||_V^2 = \left<A(w),A(w) \right> = a(w,A(w)) \leq M ||w||_V ||A(w)||_V.
	$$
	So, 
	$$
	||A(w)||_V  \leq M ||w||_V.
	$$
	
	\item $A$ is injective. 
	Indeed this is the case since, assume there is $u\neq 0$ such that $\left<A(u),v \right> = a(u,v) = 0 \forall v \in V$.
	Then taking $v=u$ and using coercivity we get,
	$$
	0 = \left< A(u),u \right> = a(u,u) \geq \gamma ||u||^2_V,
	$$
	 and so a contradiction ($u=0$).
	
	\item if $A^{-1}$ existed, then from coercivity we get,
	$$
	||w||^2_V \leq \gamma^{-1}|a(w,w)| \leq \gamma^{-1} \left< A(w),w \right>  \leq \gamma^{-1} ||A(w)||_V ||w||_V
	$$
	
	So 
	$$
	|| w ||_V \leq \gamma^{-1} ||A(w)||_{V} \;\;\; \Rightarrow ||A^{-1}||_{\mathcal{L}(V,V)} =  \gamma^{-1} .
	$$
	That is, it would be continuous, indeed, taking $w = A^{-1}(u)$, $u = A(w)$ we have,
	$$
	||A^{-1}(u)||_V \leq \gamma^{-1} ||u||_{V}
	$$
	
	\item The range of $A$, $W :=A[V]$ is closed in $V$. Let us take a Cauchy sequence of elements in $W$,
	$\{u_n\}$ we want to see that there is an element in $W$, $u$ such that $u_n \to u$ as $n\to\infty$ in the $V$ norm. 
	Since each $u_n \in W$ there exists a unique element $w_n$ such that $A(w_n) = u_n$. Coercivity implies then that $||w_n - w_m || \leq \gamma ||u_n - u_m ||_{V}$, so the sequence $\{w_n\}$ is also Cauchy. Since $V$ is complete, it converges to an element $w \in V$, furthermore, by continuity, $Lw_n = u_n \to Lw = u$.
\end{itemize}

We want to show now that $A$ is surjective, so invertible, with continuous inverse. For that it only remains to show that
$A[V] = W = V$. Assume, for contradiction this is not the case. Then let $v \in W^{\perp}$, different from $0$. We then have, for any $w \in V$

$$
0=\left< A(w),v \right>
$$
But taking $w=v$ and using coercivity,

$$
0= \left< A(v),v \right> \geq \gamma ||v||^2_V \; \Rightarrow v = 0.
$$

Thus, the problem, given $b \in V'$, find $u$ such that 

$$
a(u,v) = b(v) \;\;\forall v \in V
$$ 
transforms into, find $u \in V$ such that 

$$
a(u,v) = \left< A(u), v \right> = \left< y,v \right> = b(v)
$$
where we have used again the Riesz representation theorem to write $b(\cdot)$ as $\left< y, \cdot \right>$, and
whose solution is $u = A^{-1}(y)$.

With this theorem we can analyze non-symmetric systems like the following:

$$
\nabla \cdot (a \nabla u) + \vec{b} \cdot \nabla u + c u = f
$$

as long as $|\vec{b}|$ is small enough compared to $\underline a$ and $\underline c$.

\section{inf-sup}

The most general condition under which one can show existence of solutions to the weak problem is the following:

$$
\inf_{0 \neq w \in V} \sup_{0 \neq v \in V} \frac{a(w,v)}{||w||_V ||v||_V} \geq \gamma > 0
$$

Taking any $w \in V$ the above condition tell us that $A(w) = 0$ implies $w=0$. That is $A$ is injective. 

Taking any $v \in V$ the condition tell us that there is always a $w \in V$ such that,

$$
\left< A(w), v \right> > 0
$$

Thus, $A[V]^{\perp} = 0$ and so $A[V] = V$ and $A$ is surjective and we can solve the problem.

This is the most general condition under which the above problem is stable. Indeed assume that $A$ has a continuous inverse, then, 

\begin{align*}
||A^{-1}||_{\mathcal{L}(V,V)} &= \sup_{v \in V}\frac{||A^{-1}(v)||_V}{||v||_V} \\
	&=  \sup_{u \in V}\frac{||u||_V}{||A(u)||_V} \\
	&=  \sup_{u \in V}\frac{||u||_V \; ||A(u)||_V}{||A(u)||^2_V} \\
	&= \frac{1}{\inf_{u \in V}\frac{||A(u)||^2_V}{||u||_V \; ||A(u)||_V}} \\
	&= \frac{1}{\inf_{u \in V}\frac{\left< A(u),A(u) \right>}{||u||_V \; ||A(u)||_V}} \\
	&= \frac{1}{\inf_{u \in V}\frac{a(u,A(u)) }{||u||_V \; ||A(u)||_V}}\\
	&\geq \frac{1}{\inf_{u \in V}\sup_{v \in V} \frac{a(u,v) }{||u||_V \; ||v||_V}}.\\
\end{align*}

That is,

$$
\inf_{u \in V}\sup_{v \in V} \frac{a(u,v) }{||u||_V \; ||v||_V} \geq \frac{1}{||A^{-1}||_{\mathcal{L}(V,V)}} = \gamma.
$$

If we write the form as:

$$
a(w,v) = \left<Aw,v\right>
$$

Then we can look for the inf-sup condition directly. So we define a function 

$$
F(w,v) := \frac{  \left<Aw,v\right>}{||w|| \;||v||}
$$

Taking the derivative with respect to $v$ we get,

$$
\nabla_v F(s) = \frac{\left<Aw, s \right>}{||w|| \;||v||} - \frac{\left<Aw,v \right>}{||w||\; ||v||}\frac{\left<v,s \right>}{||v||}
$$

Thus, the supremum is reached at $v_s= v_s(w)$ given by,

$$
Aw - \frac{\left<Aw,v_s \right>}{||v_s||}\frac{v_s}{||v_s||}
$$

Taking the scalar product with $Aw$ we get, 

$$
\left<Aw,Aw \right> = \frac{\left<Aw,v_s \right>^2}{||v_s||^2}
$$

Thus, 

$$
\frac{v_s}{||v_s||} = \frac{||v_s||}{\left<Aw,v_s \right>}Aw =  \frac{Aw}{\sqrt{\left<Aw,Aw \right>}} =  \frac{Aw}{||Aw||}
$$

And the function to minimize now is:

$$
G(w) = F(w, v_s(w)) = \frac{  \left<Aw,v_s\right>}{||w|| \;||v_s||} = \frac{  \left<Aw,Aw\right>}{||w|| \;||Aw||}
$$
Instead we minimize,

$$
G^2(w) = \frac{  \left<Aw,Aw\right>}{ \left<w,w\right>}
$$

Which gives,

$$
\nabla_w G^2(s) = \frac{ 2 \left<Aw,As\right>}{ \left<w,w\right>} - \frac{2 \left<Aw,Aw\right>}{ \left<w,w\right>^2}\left<w,s\right>.
$$

So the minimum is obtained at $w_m$ solution to:

$$
A^{\dagger}Aw - \frac{\left<Aw,Aw\right>}{\left<w,w\right>} w = A^{\dagger}Aw - \lambda_m w = 0
$$

Thus, 

$$
G(w_m) = \frac{||Aw||}{||w||} = \sqrt{\lambda_m}
$$

and $\lambda_m$ is the smallest eigenvalue of $A^{\dagger}A$. Thus, the condition is that there is a gap between the smallest eigenvalue and zero (recall that the eigenvalues of this operator are all non-negative).


\example{}

Consider the system, 

\begin{align}
&\nabla \cdot \nabla \vec u + \nabla p = 0 \\
&\nabla \cdot \vec u = 0
\end{align}

The weak form is:

$$
a(w,v) = \int_{\Omega} - \nabla \vec v \cdot \nabla \vec w + \vec v \cdot \nabla p +  \vec w  \cdot \nabla q 
$$

where we have assumed that $u$ is given at the boundary and there $v$ vanishes. We shall also assume that 
$\int_{\Omega} p = 0$, same for $q$. Here $w = (\vec u,p)$, $v = (\vec v, q)$.

Using Fourier modes we get that in Fourier space the system becomes:

$$
A = \left( \begin{array}{cc}
		\delta_{ij} k^2  & k_i \\
		k_j & 0
		\end{array}
		\right)
$$  

We have, 

$$
A^{\dagger}A = 
\left( \begin{array}{cc}
		\delta_{ij} k^4  & k^2 k_i \\
		k^2 k_j & 0
		\end{array}
		\right)
$$  







\end{document}