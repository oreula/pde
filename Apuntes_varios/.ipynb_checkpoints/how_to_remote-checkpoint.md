## Instrucciones para correr en un servidor remoto

La idea es tener los archivos, julia, python y jupyter en un servidor remoto pero 
visualizar, modificar, correr y graficar desde otra computadora (notebook) con la intensión de usar servidores potentes o clusters. 

Pasos en la **computadora local:**

1. **Primera y única vez:** Crear una llave criptográfica y enviar la parte pública al servidor remoto (ver por ejemplo: https://www.ssh.com/academy/ssh/keygen)

2. Estableser la conexión por tunel: `ssh  -L 1234:localhost:1234 user@servidor.remoto` 

Mientras esté activa esta conexión todo lo que se envíe al puerto 1234 del servidor remoto será enviado al puerto 1234 de la computadora local. 


A continuación realizar los siguientes pasos en el **servidor remoto:**

1. **Primera y única vez:** Instalar julia y jupyter (puede ser la versión que se instala automática en julia) en el servidor remoto.

2. **Primera y única vez:** Instalar los archivos de su proyecto a ejecutar (usualmente simplemente clonando un repositorio git)

3. **Primera y única vez:** Ejecutar: `jupyter notebook password ` pedirá que cree un password que le pedirá desde su computadora personal.

4. Ejecutar: `jupyter notebook --no-browser --port 1234`

Pasos a realizar en la **computadora local:**

1. En un browser ir al URL: `localhost:1234`

2. Introducir el password creado en el paso *3.* del servidor remoto.

