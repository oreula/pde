\documentclass[10pt]{amsart}
%\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
%\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
%\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\title{Weak Existence for Hyperbolic Systems}
\author{Oscar Reula}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
%\section{}
%\subsection{}

\section{Introduction}

\begin{equation}
L^{\alpha}{}_{\beta} u^{\beta} := A^{\alpha a}{}_{\beta} \nabla_a u^{\beta} + B^{\alpha}{}_{\beta} u^{\beta} = J^{\alpha}
\end{equation}

We assume the system is symmetric hyperbolic, that is, there exists $h_{\alpha \beta}$ such that,

$H^a_{\alpha \beta} := h_{\alpha \gamma} A^{\gamma a}{}_{\beta}$ is symmetric and there exists $n_a$ such that
$H^n_{\alpha \beta} := H^a_{\alpha \beta}n_a$ is positive.

We are going to prove existence (and uniqueness) in a region $\Omega$ defined as follows. 
We take a smooth, compact hypersurface $\Sigma_0$ with smooth boundary and with normal $n_a$ which renders $H^n$ positive. 
We consider a one-parameter diffeomorphism that takes $\Sigma_0$ into $\Sigma_t := \Psi_t(\Sigma_0)$ such that:
i) $\forall t \in I$ the normal to $\Sigma_t$ also renders $H^n$ positive;
ii) $\Psi_t(\partial \Sigma_0) = \partial \Sigma_0$.
We shall show existence in a region $\Omega := \cap_{t=0}^T \Sigma_t$ prescribing as initial condition $u^{\alpha}|_{\Sigma_0} = 0$. 
If we were interested in a generic initial data, $u^{\alpha}|_{\Sigma_0} = f^{\alpha}$ we would then consider the equation for $\tilde{u}^{\alpha} = u^{\alpha} - \tilde{f}^{\alpha}$, where $\tilde{f}^{\alpha}$ is a suitable extension of $f^{\alpha}$ to $\Omega$.

We define a scalar product by,

\[
(v,u)_{\Omega} := \int_{\Omega} v^{\alpha} H^n_{\alpha \beta} u^{\beta}
\]

and call the corresponding norm $||u||^2_{\Omega} := (u,u)_{\Omega}$.

From the energy estimate for smooth functions which vanish at the initial surface  we get:

\[
||u||^2_{\Omega}  \leq \Gamma^2 ||J||^2_{\Omega}  = \Gamma^2 ||L(u)||^2_{\Omega}  \;\;\; \forall u^{\alpha} \in \underbar{C}^{\infty}(\Omega)
\]
Thus, 

\[
<u,v>_L := (L(v),L(u))_{\Omega}
\]
is a scalar product with norm $|||u|||^2_L = <u,u>$, we shall call $\underbar{H}(\Omega)$ to the completion of smooth functions which vanish at the initial surface, $\underbar{C}^{\infty}(\Omega)$, in the norm $|||u|||_{\Omega} := ||L(u)||_{\Omega}$. 

We now introduce norms in the dual space, the weaker one is:

\[
(v,u)_{\Omega} := \int_{\Omega} v_{\alpha} H_n^{\alpha \beta} u_{\beta} 
\]

With $H_n^{\alpha \beta}$ the inverse of $H^n_{\alpha \beta}$.

To motivate the second norm we consider now the following identity for smooth functions:

\begin{eqnarray*}
\int_{\Omega} v_{\alpha} L^{\alpha}{}_{\beta} u^{\beta} &=& \int_{\Omega} v_{\alpha} (A^{\alpha a}{}_{\beta} \nabla_a u^{\beta} + B^{\alpha}{}_{\beta} u^{\beta}) \\
&=& \int_{\Omega} [-A^{\alpha a}{}_{\beta} \nabla_a v_{\alpha} - v_{\alpha} \nabla_a A^{\alpha a}{}_{\beta}  + B^{\alpha}{}_{\beta} ] u^{\beta} + \nabla_a (v_{\alpha} A^{\alpha a}{}_{\beta}  u^{\beta}) \\
&:=& \int_{\Omega} u^{\beta} \tilde{L}_{\beta}{}^{\alpha} v_{\alpha} 
+ \oint_{\partial \Omega} (v_{\alpha} A^{\alpha a}{}_{\beta}  u^{\beta}) n_a
\end{eqnarray*}

with

\begin{equation}
\label{eqn:adjoint}
\tilde{L}_{\beta}{}^{\alpha} v_{\alpha} := -L^{\alpha}{}_{\beta} v_{\alpha} + [2 B^{\alpha}{}_{\beta} 
- \nabla_a A^{\alpha a}{}_{\beta} ]v_{\alpha} 
\end{equation}

This operator is also symmetric hyperbolic. To see this, notice that $h_{\alpha \beta}$ must be invertible (for $H^n_{\alpha \beta}$ is positive). Thus, taking $\tilde{h}^{\alpha \beta} = (h^{-1})^{\alpha \beta}$ from the right, namely 
$\tilde{h}^{\alpha \beta}  h_{\beta \gamma} = \delta^{\alpha}{}_{\gamma}$, we get,

\[
\tilde{h}^{\tau \gamma} H^a_{\gamma \beta} \tilde{h}^{\kappa \beta} = \tilde{h}^{\tau \gamma} ( h_{\gamma \alpha} A^{\alpha a}{}_{\beta} ) \tilde{h}^{\kappa \beta} = A^{\tau a}{}_{\beta} \tilde{h}^{\kappa \beta} 
\]
%
But if it is symmetric hyperbolic, then it has a similar energy estimate: 

\[
||v||^2_{\Omega} \leq \tilde{\Gamma}^2 ||\tilde{L}(v)||^2,
\]
valid for all smooth $v_{\alpha}|_{\Sigma_T} = 0$ (so we are taking initial conditions at $\Sigma_T$ and integrating backwards).
%
Thus, we can also introduce a scalar product here: 

\[
<v,u>_{\tilde{L} }:= (\tilde{L} v , \tilde{L}u)_{\Omega},
\]
%
and its corresponding norm,
\[
|||v|||^2_{\tilde{L}} := ||\tilde{L} v||_{\Omega}^2.
\]

We complete the vector space $\bar{C}^{\infty}(\Omega)$ with this norm and get a Hilbert space that we call $\bar{H}(\Omega)$. 

We are ready now to introduce the weak form of our differential equation system (with vanishing initial data):

\[
\int_{\Omega} v_{\alpha} J^{\alpha} - u^{\beta} \tilde{L}_{\beta}{}^{\alpha} v_{\alpha} = 0 \;\;\;\; \forall v_{\alpha} \in \bar{H}(\Omega).
\]
Indeed, assume that $u^{\beta}$ is smooth, then we can integrate by part, and using (\ref{eqn:adjoint}) we get,

\[
\int_{\Omega} v_{\alpha} (J^{\alpha}  - L^{\alpha}{}_{\beta} u^{\beta} ) + \oint_{\Sigma_0} (v_{\alpha} A^{\alpha a}{}_{\beta}  u^{\beta}) n_a  = 0 \;\;\;\; \forall v_{\alpha} \in \bar{H}^1(\Omega),
\]
Where we have used in the boundary integral that $v_{\alpha}$ vanishes at $\Sigma_T$.
Taking $v_{\alpha}$ also smooth, and vanishing at $\Sigma_0$, we have, 

\[
\int_{\Omega} v_{\alpha} (J^{\alpha}  - L^{\alpha}{}_{\beta} u^{\beta} )   = 0 
\]
Since the smooth functions are dense in $\bar{H}$ we get,

\[
L^{\alpha}{}_{\beta} u^{\beta} = J^{\alpha},
\]
so the equation is satisfied. 
Taking now an arbitrary $v_{\alpha}$, and noticing that only the surface integral remains in the above expression 
we get,

\[
A^{\alpha a}{}_{\beta}  u^{\beta}n_a |_{\Sigma_0}  = 0.
\]
But $A^{\alpha a}{}_{\beta}n_a$ in invertible, otherwise, $H^n_{\gamma \beta} = h_{ \gamma \alpha} A^{\alpha a}{}_{\beta}n_a$ could not be positive definite. Thus, we get $u^{\beta}n_a |_{\Sigma_0}=0$, and the boundary condition is satisfied. 

We now have all ingredients to show existence and uniqueness. Consider the following functional:

\[
\sigma_J(v) := \int_{\Omega} v_{\alpha} J^{\alpha}.
\]

This is an element of $\bar{H'}(\Omega)$, for
\[
|(v,J)_{\Omega} | \leq ||v||_{\Omega} ||J||_{\Omega} \leq \tilde{\Gamma}  ||J||_{\Omega} |||v|||_{\Omega} 
\]

Thus, from Riesz representation theorem we know there exists a unique  $w_{\alpha}$ in $\bar{H}(\Omega)$ such that,

\[
\sigma_J(v) = \int_{\Omega} v_{\alpha} J^{\alpha} = <v, w> _{\Omega} =  \int_{\Omega} \tilde{L}(v)_{\alpha} H_n^{\alpha \beta} \tilde{L}(w)_{\beta}.
\] 

Therefore, 

\[
u^{\alpha} = H^{\alpha \beta}_n \tilde{L}_{\beta}{}^{\gamma} w_{\gamma}
\]
satisfies the weak equation.

Notice that the solution found is just in $L^2(\Omega)$ so at this point we don't even know whether it satisfies the boundary condition we would like it to satisfy, namely ($u|_{\Sigma_0} = 0$).


\end{document}


