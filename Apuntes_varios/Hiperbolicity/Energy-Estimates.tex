\documentclass[10pt]{amsart}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\title{Energy Estimates}
\author{Oscar Reula}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
%\section{}
%\subsection{}

\section{Introduction}

Central to the proof of existence, uniqueness and continuity w.r.t. initial data of hyperbolic equations are the so called energy estimates. 
They are referred  to as \textsl{a priori} estimates, for the assume a solution exists for every smooth initial data set. 
They consists in defining an appropriate norm, called the \textsl{energy norm} for solutions at a given, arbitrary time, and showing that its time derivative is bounded by a function of itself. That is, 

\[
\frac{d \mathcal{E}}{dt} \leq \mathcal{F}(\mathcal{E}).
\]
Furthermore, $\mathcal{F}$ is differentiable, and $\mathcal{F}(0) = 0$.
It is not difficult to see that if $\mathcal{Y}(t)$ is a solution to 

\[
\frac{d \mathcal{Y}}{dt} = \mathcal{F}(\mathcal{Y})\;\;\;\; \mbox{with}\;\; \mathcal{Y}(0) = \mathcal{E}(0),
\]
%
then, 

\[
\mathcal{E}(t) \leq \mathcal{Y}(t)
\]
on the whole interval of existence of $\mathcal{Y}(t)$, and so there is a finite time interval where the energy, namely the norm of the solution is bounded. The properties of $\mathcal{F}$ imply that solutions are smooth functions of initial data sets in the energy norm. Since $\mathcal{F}(\cdot)$ is a differentiable function, $\mathcal{Y}(t)$ is a differentiable function of its initial data, and so of $\mathcal{E}(0)$, thus, $\mathcal{E}(t)$ is bounded by some differentiable function of its initial value.

We start with a couple of representative examples.

\section{The Advection Equation}


We consider first the simplest case, the advection equation in the circle:

\[
\partial_t u = a \partial_x u, \;\;\;\; u(t=0,x) = f(x)
\]
%
where $a$ is a constant. 
We define the \textsl{energy},

\[
\mathcal{E}(t) := \int_{S^1} |u(t,\cdot)|^2 \;dx 
\]

Taking a time derivative we get, 

\begin{eqnarray*}
\frac{d\mathcal{E}(t)}{dt} &=& \int_{S^1} [ \bar{u} \partial_t u + \partial_t \bar{u} u ]\;  dx \\
                                        &=& \int_{S^1} [ \bar{u} a \partial_x u + \bar{a} \partial_x \bar{u} u ]\;  dx \\
                                        &=& \int_{S^1} [ a \partial_x (\bar{u} u) + (\bar{a} - a) u \partial_x \bar{u}  ]\;  dx \\
                                        &=& \int_{S^1} (\bar{a} - a) u \partial_x \bar{u}  \;  dx \\
\end{eqnarray*}
% 
where in the last line we have integrated away the total derivative. Thus, if $a$ is real we have that the energy is conserved. Otherwise, the last term can not be integrated away and the energy depends on derivative of the fields. In that case we know there is no continuity w.r.t. the initial data: we can make a Cauchy sequence of smooth function ion the norm $L^2$, and even in the norm $C^0$, but when taken as initial data, the corresponding solutions grow without bound for any $t\neq 0$. Energy conservation implies that 

\[
||u(t)||^2_{L^2} = \mathcal{E}(t) =  \mathcal{E}(0) = ||u(0)||^2_{L^2} = ||f ||^2_{L^2}.
\]
Thus, in the $L^2$ norm the solution at any given time is a continuous function of the initial data \textsl{in the same norm}. 

Since $u^{(1)} := \partial_x u$ satisfies the same equation, $\partial_t u^{(1)} = a \partial_x u^{(1)}$ we also have, 

\[
||u^{(1)}(t)||^2_{L^2} = ||u^{(1)}(0)||^2_{L^2} = ||\partial_x f ||^2_{L^2}.
\]

In the same way we see that if the data is in the Sobolev space $H^m$, 
\[
||\partial^m_x u(t,\cdot)||^2_{L^2} = ||u^{(m)}(t)||^2_{L^2} = ||u^{(m)}(0)||^2_{L^2} = ||\partial^m_x f ||^2_{L^2},
\]
%
then the solution at any given time is also in that space and we have continuity on any Sobolev space for which the data belongs to. Notice that in all the procedure we have been doing operations, like taking derivatives, and multiplying different functions and integrating by parts as if they were smooth. This is indeed what we have assumed, but then, after finishing the estimate, we can take Cauchy sequences in both spaces (assuming a smooth solution exists for each smooth initial data in the sequence) and then taking the limit, which are guaranty to exists because the density of smooth functions and the the completeness of spaces.



\section{Burgers Equations}


We consider Burgers equation, again in the circle:

\[
\partial_t u = u \partial_x u, \;\;\;\; u(t=0,x) = f(x) \;\; \mbox{a real function}
\]
% 
We define the \textsl{energy},

\[
\mathcal{E}(t) := \int_{S^1} u(t,\cdot)^2 \;dx 
\]

Taking a time derivative we get, 

\begin{eqnarray*}
\frac{d\mathcal{E}_0(t)}{dt} &=& \int_{S^1} 2 u \partial_t u \;  dx \\
                                        &=& \int_{S^1} 2 u^2 \partial_x u \;    dx \\
                                        &=& \int_{S^1} \frac{2}{3} \partial_x(u^3)\;  dx \\
                                        &=& 0
\end{eqnarray*}
% 
Note that we could have used any positive power equal or grater than $1$ and gotten the same conservation.
This equation has an infinite number of conserved quantities! Nevertheless we know that smooth solutions exist only for a limited time span! So, even when these quantities seems to be preserved for an infinite time, the solution nevertheless only lives and is unique for a short time.

The finite differentiability appears as soon as we consider norms for derivatives, defining again $v := \partial_x u$, this time we get, 

\[
\partial_t v = (u \partial_x u)_x = v^2 + u \partial_x v,
\]
%
and,

\[
\mathcal{E}_1(t) := \int_{S^1} v(t,\cdot)^2  \;dx 
\]

\begin{eqnarray*}
\frac{d\mathcal{E}_1(t)}{dt} &=& \int_{S^1} 2 v \partial_t v \;  dx \\
                                        &=& \int_{S^1} 2 [v^3 +  v u \partial_x v] \;    dx \\
                                        &=& \int_{S^1} v^3 \;  dx \\
                                        &=& \sup_{x\in S^1}\{|v|\} \int_{S^1} v^2 \;  dx \\
                                        &\leq& \sup_{x\in S^1}\{|\partial_x u |\} \mathcal{E}_1(t)
\end{eqnarray*}
% 

So we still do not have an estimate with the properties we need, in fact, the equation for $v$ says that,
integrating along the vector $l^a = (1,u)$ from any point, $x_0$, at the initial surface we will be solving,

\[
\frac{d}{d\lambda} v = v^2
\]
with has as a solution, 

\[
v(t) = \frac{v_0}{\lambda v - 1},
\]
where $v_0$ is the value of $v=\partial_x u$ at $(t=0,x_0)$. 
So, if $v_0> 0$ the solution blows up at $\lambda = \frac{1}{v_0}$ (if not to the past). 
Notice that the existence time is bounded by the maximum of the space derivative of $u$, 
\[
|\lambda_{min}| = \frac{1}{v_{max}}, \;\;\;\;\;\; \mbox{with} \;\;\;\;\;\; v_{max} := \sup_{x \in S^1}\{|\partial_x u |\}
\]

To get the desired estimate we need to control also the maximum of the derivative while the solution is smooth.
To do that we consider another space derivative, $w := \partial^2_x u = \partial_x v$.

this time we get, 

\[
\partial_t w = (v^2 + u \partial_x v)_x =  3 v w  + u \partial_x w,
\]
%
and,

\[
\mathcal{E}_2(t) := \int_{S^1} w(t,\cdot)^2  \;dx 
\]

\begin{eqnarray*}
\frac{d\mathcal{E}_2(t)}{dt} &=& \int_{S^1} 2 w \partial_t w \;  dx \\
                                        &=& \int_{S^1} 2w [3 v w  + u \partial_x w] \;    dx \\
                                        &=& \int_{S^1} [6v w^2 + 2u w \partial_x w] \;  dx \\
                                        &=& \int_{S^1} [6v w^2 + u \partial_x w^2] \;  dx \\
                                        &=& \int_{S^1} [6v w^2 -  v w^2 + \partial_x (v w^2)] \;  dx \\
                                        &=& 5 \sup_{x\in S^1}\{|v|\} \int_{S^1} w^2 \;  dx \\
                                        &\leq& \sup_{x\in S^1}\{|\partial_x u |\} \mathcal{E}_2(t).
\end{eqnarray*}
% 
So, again we get the same condition on the point-wise growth of the first derivative. But this time we can use Sobolev lemma which asserts there exists a constant $C_s$ such that,

\[
\sup_{x\in S^1}\{|\partial_x u |\} \leq C_s ||u||_{H^2}.
\]
But the sum of the three energies is just $||u||^2_{H^2}$, thus, summing the estimates we get, 

\[
||u(t)||^2_{H^2} \leq 5 C_s (||u||_{H^2})^{3/2}.
\]

And this really is an energy estimate.


\section{Symmetric Hyperbolic Systems}

We consider now systems of waves, that is first order systems of the form,

\[
\partial_t u^{\alpha} + A^{\alpha j}{}_{\beta} \partial_j u^{\beta} = J^{\beta}.
\]

Recall that hyperbolicity is defined as the property that the eigenvalues of $A^{\alpha j}{}_{\beta} \partial_j k_j$, for arbitrary real co-vector $k_j$, are real. A system is strong-hyperbolic when the above matrices have a complete set of eigenvalues. Together with some uniformity conditions strong-hyperbolicity implies well posednes. 
We consider now a subclass of hyperbolic well-posed systems, the symmetric ones. They comprise most of the systems of physical interest (notably not the simpler versions of charged fluids), and the have the property that they define energies in physical space.

We shall call a hyperbolic system symmetric if there exist a symmetric bilinear form $h_{\gamma \alpha } = h_{\alpha \gamma}$, positive definite so that, the matrices $h_{\gamma \alpha }A^{\alpha j}{}_{\beta}$ are symmetric.
We shall refer to these bilinear forms as the symmetrizers of $A^{\alpha j}{}_{\beta}$.

Before proceeding we write a covariant version of these systems so that we can make some geometrical statements in a more strait-forward manner. 
In the coordinates in which we are expressing the above system we define:

\[
A^{\alpha a}{}_{\beta} = (\delta^{\alpha}{}_{\beta}, A^{\alpha j}{}_{\beta} ) \;\;\;\; a = 0, 1, 2, 3, \;\;\; j = 1, 2, 3,
\]
%
and
\[
x^a = (x^0, x^1, x^2, x^3) = (t, x^1, x^2, x^3),
\]
%
we can express the system as, 


\[
A^{\alpha a}{}_{\beta} \partial_a u^{\beta} = J^{\beta}.
\]

We can now forget about the original coordinate system and think of these equations in a arbitrary coordinates, without any reference to a particular time or space-slice. 

For this covariant system symmetric-hyperbolicity means that there exists a symmetric bilinear form $h_{\gamma \alpha } = h_{\alpha \gamma}$, and a co-vector $n_a$ such that the matrices $h_{\gamma \alpha }A^{\alpha a}{}_{\beta}$ are symmetric, and $h_{\gamma \alpha }A^{\alpha a}{}_{\beta}n_a$ is positive. (Note that we don't impose the positivity of $h_{\alpha \gamma}$. The positivity of $h_{\gamma \alpha }A^{\alpha a}{}_{\beta}n_a$ does restrict the possible symmetrizers, $h_{\alpha \gamma}$, since that requieres them to be invertible.)

Given a solution of the system, $u^{\alpha}$ consider now the vector 

\[
P^a := u^{\gamma}h_{\gamma \alpha }A^{\alpha a}{}_{\beta} u^{\beta}.
\]

We have, 

\begin{eqnarray*}
\partial_a P^a &=& \partial_a (u^{\gamma}h_{\gamma \alpha }A^{\alpha a}{}_{\beta} u^{\beta}) \\
		      &=& (\partial_a u^{\gamma})h_{\gamma \alpha }A^{\alpha a}{}_{\beta} u^{\beta} +
		      u^{\gamma}h_{\gamma \alpha }A^{\alpha a}{}_{\beta} \partial_a u^{\beta} + 
		      u^{\gamma} u^{\beta} \partial_a (h_{\gamma \alpha }A^{\alpha a}{}_{\beta})\\
		      &=& (\partial_a u^{\gamma})h_{\beta \alpha }A^{\alpha a}{}_{\gamma} u^{\beta} +
		      u^{\gamma}h_{\gamma \alpha }A^{\alpha a}{}_{\beta} \partial_a u^{\beta} + 
		      u^{\gamma} u^{\beta} \partial_a (h_{\gamma \alpha }A^{\alpha a}{}_{\beta})\\
		      &=& 2h_{\beta \alpha }u^{\beta} J^{\alpha} +
		      u^{\gamma} u^{\beta} \partial_a (h_{\gamma \alpha }A^{\alpha a}{}_{\beta}),
\end{eqnarray*}
%
where in going from the second to the third line we have used the symmetry of $h_{\gamma \alpha }A^{\alpha a}{}_{\beta}$, and then the equation.
An important point is the, except for the cuasi-linear case, the right hand side does not have derivatives of $u^{\alpha}$.

With this expression we shall build the energy estimates for systems. For that we consider a surface $\Sigma_0$ whose normal $n_a$ satisfies the positivity condition and a one parameter families of surfaces $\Sigma_t$, $t \in [0,T]$ such that:
i) $\Sigma_{t=0} = \Sigma_0$, 
ii) $\Cup_{t  \in [0,T]} \Sigma_{t} = \Omega$ is a compact region of space-time, (in particular this means that a all this surfaces coincide outside a compact region),
iii) The normal to all these surfaces $n_a$ satisfies $P^an_a > 0$ when a suitable orientation is taken for each of them on a continuous way.

Note that if we can choose a continuous orientation for $n_a$ at the initial surface, $\Sigma_0$, then there will be a neighborhood of it in which we can make our construction. This is so because the set of positive definite bilinear forms is open, and so small perturbations of $P^a$ and of $n_a$ result in positive definite expressions. 

Using the divergence theorem we obtain, 

\[
\int_{\Omega} \partial_a P^a dV = \int_{\partial \Omega} P^a n_a dS =   \int_{\Sigma_T} P^a n_a dS +  \int_{\Sigma_0} P^a n_a dS
\]
%
where $n_a$ is the outer normal to the surfaces. We assume the outer normal to $\Sigma_T$ satisfies $P^a n_a > 0$ and so use this orientation. So that at the initial surface we change $n_a \to -n_a$ and change the sign of the integral.
Thus we get, 

\[
\int_{\Sigma_T} P^a n_a dS = \int_{\Sigma_0} P^a n_a dS + \int_{\Omega} [2h_{\beta \alpha }u^{\beta} J^{\alpha} +
		      u^{\gamma} u^{\beta} \partial_a (h_{\gamma \alpha }A^{\alpha a}{}_{\beta})] dV
\]

Using the scalar product and norm given by,

\[
(u, v)_{\Sigma_t} := \int_{\Sigma_t} u^{\gamma}h_{\gamma \alpha }A^{\alpha a}{}_{\beta}n_a v^{\beta}  dS, \;\;\;\; ||u||^2_{\Sigma_t} = (u,u)_{\Sigma_t}
\]
 
(lower indices are here raised using the inverse of $h_{\gamma \alpha }A^{\alpha a}{}_{\beta} n_a$), we obtain the estimate:

\[
\int_{\Sigma_T} P^a n_a dS \leq \int_{\Sigma_0} P^a n_a dS + \int_0^T [\epsilon C_1 ||u||^2_{\Sigma_t} 
+   \frac{C_1}{\epsilon} ||J||^2_{\Sigma_t} + C_2||u||^2_{\Sigma_t}] dt
\]



















\end{document}