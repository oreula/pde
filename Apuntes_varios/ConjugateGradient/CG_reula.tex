\documentclass[11pt]{amsart}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\usepackage{physics}

%%%%
%%%% The next few commands set up the theorem type environments.
%%%% Here they are set up to be numbered section.number, but this can
%%%% be changed.
%%%%

\newtheorem{thm}{Theorem}[section]
\newtheorem{prop}[thm]{Proposition}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{cor}[thm]{Corollary}


%%
%% If some other type is need, say conjectures, then it is constructed
%% by editing and uncommenting the following.
%%

%\newtheorem{conj}[thm]{Conjecture} 


%%% 
%%% The following gives definition type environments (which only differ
%%% from theorem type invironmants in the choices of fonts).  The
%%% numbering is still tied to the theorem counter.
%%% 

\theoremstyle{definition}
\newtheorem{definition}[thm]{Definition}
\newtheorem{example}[thm]{Example}

%%
%% Again more of these can be added by uncommenting and editing the
%% following. 
%%

%\newtheorem{note}[thm]{Note}


%%% 
%%% The following gives remark type environments (which only differ
%%% from theorem type invironmants in the choices of fonts).  The
%%% numbering is still tied to the theorem counter.
%%% 


\theoremstyle{remark}

\newtheorem{remark}[thm]{Remark}


%%%
%%% The following, if uncommented, numbers equations within sections.
%%% 

\numberwithin{equation}{section}


%%%
%%% The following show how to make definition (also called macros or
%%% abbreviations).  For example to use get a bold face R for use to
%%% name the real numbers the command is \mathbf{R}.  To save typing we
%%% can abbreviate as

\newcommand{\R}{\mathbf{R}}  % The real numbers.

%%
%% The comment after the defintion is not required, but if you are
%% working with someone they will likely thank you for explaining your
%% definition.  
%%
%% Now add you own definitions:
%%

%%%
%%% Mathematical operators (things like sin and cos which are used as
%%% functions and have slightly different spacing when typeset than
%%% variables are defined as follows:
%%%

\DeclareMathOperator{\dist}{dist} % The distance.



%%
%% This is the end of the preamble.
%% 

\title{Conjugate methods}
\author{Oscar Reula}
%\date{}                                           % Activate to display a given date or no date

\begin{document}

\begin{abstract}

Here we discuss the conjugate methods, and particularize for conjugate gradient, for solving the system $Ax=b$.
This notes are based on the following notes, \cite{Meyer, Arnold, Gutknecht, Shewchuk, Runar, Shen}.

\end{abstract}

\maketitle


\section{Introduction}

We want to solve the problem,

\begin{equation}
\label{eqn:AXB}
Ax=b
\end{equation}
%
when $A$ is a symmetric, positive definite matrix, (SPD) that is, there is a scalar product $<\cdot,\cdot>$, such that $<y,Az> = <Ay,z>$ and $<z,Az> = 0$ sii $z=0$. 

We want to solve it using efficient methods based upon the natural minimizing sequence, that is the Conjugate Gradient Method, but we shall do it in a way so that the 
principles used to construct the method are useful for other methods where the matrix does not have the SPD property.


\subsection{Variational Principle}

Let $F:\R^n \to \R$ given by,

\begin{equation}
\label{eqn:F}
F(y) := \frac{1}{2} <y,Ay> - <y,b>,
\end{equation}

We shall see that $F$ as a unique minimum and that minimum satisfies \ref{eqn:AXB}.
Since $A$ is SPD, $F$ is bounded by below and so by continuity has at least one minimum, let us call it $x$. 
Since it is a minimum, then defining $\gamma(\lambda) = x + \lambda s$, $s$ and arbitrary vector. We must have,

\[
0 = \frac{dF}{d\lambda}|_{\lambda = 0} = <s, Ax-b>,
\]
Since $s$ is arbitrary  we must have, $Ax=b$. Let now $y$ be any other vector, then

\begin{eqnarray*}
F(y) &=& \frac{1}{2}<y,Ay> - <y,b> \\ 
       &=& \frac{1}{2}<y-x+x,A(y-x+x)> - <y-x+x,b> \\ 
       &=& \frac{1}{2}<y-x,A(y-x)> + F(x) + <y-x,Ax> - <y-x,b> \\
       &=& \frac{1}{2}<y-x,A(y-x)> + F(x) \\  
       &>& F(x)     
\end{eqnarray*}
Which shows that $x$ is unique. Furthermore it shows that the level sets of $F$ are ellipsoids in $\R^n$.
This is important, for if we cut en ellipsoid by a subspace of any dimension, then the restriction to that subspaces are also ellipsoids. 
Thus if we minimize in subspaces at each dimension we have a similar problem: find the ellipsoid center.

\section{Iterations}

To solve big systems, more than a million dimensions, conventional direct methods are out of the question. In particular because if we are solving
and approximation to a continuum system using a discrete one, there is an inherent error, so solving it more accurate than this error is useless. 
This is where iterative methods enter the game.
We shall look at the simplest iteration procedure, Richardson Interpolation.

The idea is to cast the equation as a fixed point of an iteration map. Richardson map is given by,

\[
x_{i+1} = x_{i} - \omega(A x_i - b) := G x_i - \omega b_i \;\;\;\;\;\; \omega >0
\]

The fix point is, 

\[
x = x - \omega(Ax -b).
\]
Thus it satisfies \ref{eqn:AXB}.
Notice that if $A$ is the discretization of the Laplacian in some region, then what we are solving is the heat equation with a source $b$ and $\omega = \delta t$ with Euler's method.
If we call $e_i : x_i - x$, the error at step $i$, then it satisfies,

\[
e_{i+1} = e_{i} - \omega A e_i  := G e_i  \;\;\;\; e_i = G^i e_0.
\]
 
Therefore the iteration will converge (for arbitrary initial data) if $||e_i|| \leq ||G||^i ||e_0|| \to 0$, which is so if $||G|| < 1$.
That is,

\[
||G|| = || I - \omega A|| < 1.
\]

So far the norm used here is a generic one, since we are in a finite dimensional space we know they are all equivalent. But using the Lyapunov trick we know that there is one
for which  if  the spectral   radius $\rho(G) := \max\{|\lambda^G_i|\} < 1$ then $||G|| < 1$.  Thus we need to look just at the spectral radius. 
Thus, the condition is $\max\{|1 - \omega \lambda^A_{max}|, |1 - \omega \lambda^A_{min}| \} < 1$. 
Which becomes the condition (for any eigenvalue, and suppressing the index A),

\[
1-\sqrt{1-\omega^2 \Im(\lambda)^2} < \omega \Re(\lambda) < 1 + \sqrt{1- \omega^2 \Im(\lambda)^2}
\]
Note, in the usual notation for the Euler convergence region the operator $A$ has the oposite sign.

The optimal value for $\omega$, that is the value for which the amplification factor es the smallest, is the one for which both expressions for the maximum value coincide for the biggest and lowest eigenvalues, namely the $\omega$ for which (we take here $\Im(\lambda)=0$, and square the amplification factor), 

\begin{eqnarray*}
0 &=&  (1 - \omega \lambda_{max})^2 - (1 - \omega \lambda_{min})^2 \\
&=& -2 \omega(\lambda_{max} - \lambda_{min}) + \omega^2 (\lambda_{max}^2 - \lambda_{min}^2) \\
&=& \omega((\lambda_{max} - \lambda_{min}) (-2 +  \omega^2 (\lambda_{max}^2 + \lambda_{min}^2),
\end{eqnarray*}
Namely, 
\[
\omega = \frac{2}{\lambda_{max} + \lambda_{min}} 
\]
which gives
\[
\rho(G) = \frac{\lambda_{max} - \lambda_{min}}{\lambda_{max} + \lambda_{min}} = \frac{\kappa -1}{\kappa + 1} \approx 1 - \frac{2}{\kappa}
\]
where $\kappa := \frac{\lambda_{max}}{ \lambda_{min}} = \frac{||A||}{||A^{-1}||}$ is called the spectral condition number, and plays an important role in diagnosing how difficult is to invert a matrix.
When this number is big, as is in the case of the approximations to a partial differential equation, the iteration converges as follows:
If we want to reach a given error, $\epsilon$ then we shall need $N$ iterations according to:

\[
N = \frac{|\log(\frac{\epsilon}{||e_0||})|}{|\log(\rho(G))|} \approx \frac{|\log(\frac{\epsilon}{||e_0||})|}{|\log(1-\frac{2}{\kappa})|} \approx |\log(\frac{\epsilon}{||e_0||})| \frac{\kappa}{2}
\]
Thus, the number of steps to reach a given precision scales as the conditional number of the matrix. For the finite difference of the Laplacian that number grows like $M^2$ where $M$ is the number of grid points on each space direction so we see that we need the square of that number as iterations. Something familiar from the CFL condition for parabolic equations.


\section{Krylov Spaces}

 One important aspect of the previous iteration is that the approximation is on the space generated by 
 
 \[
 K_i := x_0 + Span\{r_0, A r_0, \ldots, A^{i-1}r_0\}
 \]
 
 To see this define $r_i = Ax_i - b$, that is the residual of the equation we want to solve. 
 Then, Richardson iteration is given by,
 
 \[
 x_{i+1} = x_i + \omega(Ax_i -b) = x_i + \omega r_i
 \]
 so that 
 
 \[
 x_{i} = x_0 + Span\{r_0, r_1, \ldots, r_{i-1}\}.
 \]
 
 On the other hand, 
 
 \[
 r_{i+1} = Ax_{i+1} - b = A(x_i + \omega r_i) -b = r_i + \omega Ar_i = (I+ \omega A) r_i
 \]
 so that 
 
 \[ 
 r_{i} = Span\{r_0, Ar_0, \ldots A^{i-1}r_0\}\;\;\;\; \mbox{and} \;\; x_{i} = x_0 + Span\{r_0, Ar_0, \ldots A^{i-1}r_0\}
 \]
 
The spaces of this form, namely the iteration of an operator on a given vector are known as Krylov spaces. 
They naturally appear in this problem for the following reason:

The Cayley-Hamilton theorem states that

\[
p(A) = o
\]
%
Where $p(\lambda) = det(A - \lambda I)$ is the characteristic polynomial of $A$ and $p(A)$ is defined by replacing on each appearance of 
$\lambda$ in $p(\lambda)$ by $A$.
Since 

\[
p(\lambda) = \prod_{j=1}^d (\lambda_j - \lambda)^{m_j} := \sum_{i=0}^{n} \alpha_i \lambda^i
\]
%
where the product is on the different Jordan blocks of the matrix of eigenvalue $\lambda_j$ and sizes $m_j$. 
Notice that $\alpha_0 = \prod_{j=1}^n \lambda_j$ is different from zero if the matrix is invertible.
Thus, in that case,

\[
0 = A^{-1}p(A) = \alpha_0 A^{-1}  + \sum_{i=1}^n \alpha_i A^{i-1}
\]
%
or
\[
A^{-1} = \frac{-1}{\alpha_0} \sum_{i=1}^n \alpha_i A^{i-1} 
\]
% 
Thus,

\[
x = A^{-1}b \in K_{n-1}(A,b) = Span\{b, Ab, \ldots, A^{n-1} b\}
\] 
 
It is important to notice the following fact, for diagonalizable matrices all Jordan blocks are one dimensional and if there are repeated eigenvalues then 
the minimal polynomial which vanishes on $A$ is smaller that $p(A)$ and it is given by

\[
q(A) = \prod_{j=0}^{\hat{d}} (A - \lambda_j I)
\]
where the product only goes once for each different eigenvalue. Thus, when this happens the corresponding Krylov space is of lower dimensionality.
This also happens if one chooses, for instance, $b$ to be an eigenvalue of $A$, in that case the space is just one-dimensional.
Therefore it is important to stay, along the iterations, on the natural Krylov spaces of the problem.

\section{Steepest Descent}

SPD matrices arise very often in physics for in general they represent equations minimizing some form of energy. In general cases the equations are non-linear
and so we can only expect to find the simpler problems we are dealing with near local minima. 
Thus, near those minima the situation is of a SPD minimizing type:

We want to minimize the function defined in \ref{eqn:F}. One way of doing this is to fall along the steepest descend, that is along the negative of the gradient of $F$.

Let us see what the gradient is. We first define the differential of $F$, $dF$ this is an element of the dual of $R^n$ such that, 

\[
dF_y(s) := \frac{dF}{d\lambda}(y + \lambda s)|_{\lambda=0} = <s, Ay - b> = <s, r_y>
\]

Thus, using the scalar product $< \;, \;>$ we identify $r_y := \nabla F(y)$, which is a vector. 


That is, move from the point 
$x_i$ along the direction $-\grad F(x_i) = - (Ax_i - b) = -r_i$, so the curve would be $\gamma(\lambda) := x_i - \lambda r_i$ until we reach some minimum along that direction.
At that point we must have,

\[
0 = \frac{dF}{d\lambda}|_{\lambda_{i}}(r_i) = -<r_i, A(x_i - \lambda_{i} r_i) -b> = - <r_i, r_i - \lambda_{i} A r_i>,
\]
%
from which we conclude that $\lambda_{i} = \frac{<r_i, r_i>}{<r_i, A r_i>}$.
Then slide down from there again along the $-\grad F$ which necessarily 
at that point points along a different direction. The scheme is then,

\begin{eqnarray*}
\mbox{\textbf{Choose,} } \; x_0 \;\;\;\;\; \\
\mbox{\textbf{for }}\; i= 1, \ldots \\
r_{i} &=& Ax_i - b  \\
\lambda_{i} &=& \frac{<r_i, r_i>}{<r_i, A r_i>} \\
x_{i+1} &=& x_i - \lambda_{i} r_{i} \\
\mbox{\textbf{Return}} \;\;\;\;\;\;\;\
\end{eqnarray*}

The convergence of this method is similar to the one for Richardson interpolation, namely the number of steps for a given accuracy scales as $\kappa$.

\begin{figure}[htbp]
\begin{center}
%\input{disperions.pdf}
%\input{dispersions.png}
\centerline{\includegraphics[width=8cm, height=6cm]{steep_descend.pdf}}
\caption{The Steepest decent method.}
\label{fig:SD}
\end{center}
\end{figure}



\section{Conjugate methods}

The above method is not optimal because we have not used the appropriate scalar product to define the gradient.


Assume you have minimized $F$ in a given subspace $K_i$ and let the minimum in that subspace be $x_i$. 
Take now any vector in $t_i \in K_i$ and $\gamma(\lambda) = x_i + \lambda t_i$, the fact that $x_i$ is a minimum implies,

\begin{equation}
\label{eqn:grad}
0 = \frac{dF}{d\lambda}|_{\lambda = 0} = <t_i, Ax_i-b> = <t_i, r_i> = <t_i, A(x_i-x)> = <t_i,Ae_i> ,
\end{equation}
Where we have defined the residual as $r_i = Ax_i - b $ and the error as $e_i = x_i -x$.
So, we have, 

\begin{equation}
\label{eqn:perp_cond}
r_i \perp K_i \;\;\;\;\;\;
e_i \perp_A K_i
\end{equation}

Thus, it is clear that to further minimize the error we should enlarge the subspace so that the new direction is also A-orthogonal to $K_i$. 
Let us take any such direction and, calling it $s$, consider $K_{i+1} = Span\{K_i ,s\}$.

We shall prove now that to find the minimum in $K_{i+1}$ we only need to look for it along $s$.
That is,

\[
\min_{y \in K_{i+1}} F(y) = \min_{\lambda \in \R}F(x_i + \lambda s).
\]

To see this consider, 

\begin{eqnarray*}
\min_{y \in K_{i+1}} F(y) &=& \min_{y \in K_i, \lambda \in \R} F(y + \lambda s) \\
				     &=& \min_{y \in K_i, \lambda \in \R} \frac{1}{2}< y + \lambda s, A(y + \lambda s) - 2b> \\
				     &=& \min_{y \in K_i, \lambda \in \R} 
				     [ \frac{1}{2}< y, A(y) - 2b> + \frac{1}{2}< \lambda s, A(\lambda s) - 2b>  + \frac{\lambda}{2}< y, A(s)> + \frac{\lambda}{2}<s, A(y)>] \\
                                       &=& \min_{y \in K_i, \lambda \in \R} [F(y) + \lambda[\frac{\lambda}{2} <s,As> - <s,b>]] \\
                                       &=& \min_{y \in K_i} F(y) + \min_{\lambda \in \R} [\frac{\lambda^2}{2} <s,As> - \lambda<s,b>]\\
                                       &=& F(x_i) - \frac{1}{2}\frac{<s,b>^2}{<s,As>^2} \\
                                       &=& \min_{\lambda \in \R} F(x_i + \lambda s).
\end{eqnarray*}
Where we have only used that $s \perp_A K_i$ and minimized choosing 
$\lambda = \frac{<s,b>}{<s,As>} = -\frac{<s,r_i>}{<s,As>}$.
Thus, choosing a set of $n$ mutually A-orthogonal directions we span $R^n$ and so line search along this directions allows us to reach the minimum of F and so find the solution for
\ref{eqn:AXB}. 


Any choice would do, in fact, assume you have a complete set of A-orthonormal vectors, $K_{n}$, then we can expand,

\[
x = \sum_{i=0}^{n-1} x^i s_i,
\]

and then, 

\[
<s_j, Ax> = \sum_{i=0}^{n-1} x^i <s_j, A s_i> = x^j <s_j, A s_j> = <s_j, b>.
\]

That is,

\[
x^j = \frac{<s_j, b>}{ <s_j, A s_j>}
\]

So one can imagine that the main computational cost is to find $K_{n}$. But there are optimal choices of such a base in terms of computational costs.

\section{Conjugate Gradient Method}

In this method one chooses a bases of residuals, and uses them to build step by step the A-orthogonal base.
We choose $x_0=0$ for simplicity.

\begin{eqnarray*}
r_0 &=& b \\
s_0 &=& - r_0 \\
\lambda_i &=& -\frac{<r_i,s_i>}{<s_i,A s_i>} \\
x_{i+1} &=& x_{i} + \lambda_i s_i \\
r_{i+1} &=& A x_{i+1} - b = r_i + \lambda_i A s_i \\
s_{i+1} &=& - r_{i+1} + \sum_{j=0}^{i} \frac{<s_j, A r_{i+1}>}{<s_j,As_j> }s_j
\end{eqnarray*}

This sequence has the following properties:

\begin{enumerate}
\item $K_i = Span\{s_0, s_1, \ldots, s_{i-1}\} = Span\{r_0, r_1, \ldots, r_{i-1}\}$
\item $<r_i,r_j>=0$, $i\neq j$
\item There exists $m \leq n$ such that $K_1 \subsetneq K_2 \subsetneq \ldots K_m = K_{m+1} = \ldots K_n$, and $x_0 \neq x_1 \neq \ldots \neq x_m = x_{m+1} = \ldots = x_m = x$
\item For $i \leq m$ $\{ s_0, s_1, \ldots, s_{i-1}\}$ is an A-orthogonal base for $K_i$, while  $\{r_0, r_1, \ldots, r_{i-1}\}$ is an $l^2$-orthogonal base.
\item $<s_j, A r_{i+1}>=0, \;\;\; 0\leq j < i $, $<s_i, A r_{i+1}>=\frac{<r_{i+1},r_{i+1}>}{\lambda_i}$
\end{enumerate}

The first is clear from the recurrence relation. The second follows from the first and \ref{eqn:perp_cond}. The third and fourth are also immediate: certainly that $m$ exists, in that case it is either $n$ or is smaller, if it is smaller then $K_m=K_{m+1}$ and so $r_m$ is perpendicular to itself, so it vanishes. After that the sequence becomes trivial.
For the last one use for $i=j$ 
we get, 

\[
<s_i, A r_{i+1}> = <A s_i, r_{i+1}>=\frac{1}{\lambda_i}<r_{i+1} - r_{i},r_{i+1}>=\frac{1}{\lambda_i}<r_{i+1},r_{i+1}>
\]

if $j \leq i-1$, $j + 1 < i+1$, then,

\[
<s_j, A r_{i+1}> = <A s_j, r_{i+1}>=\frac{1}{\lambda_j}<r_{j+1} - r_{j},r_{i+1}>=0
\]

Thus, the recurrence relation for $s_{i+1}$ becomes,

\[
s_{i+1} = - r_{i+1} + \frac{1}{\lambda_i}\frac{<r_{i+1},r_{i+1}>}{<s_i,A s_i>} s_i = - r_{i+1} + \frac{<r_{i+1},r_{i+1}>}{<r_{i},r_{i}>} s_i.
\]

Thus the whole recurrence becomes, 

\begin{eqnarray*}
\mbox{\textbf{Choose,} } \; x_0 = 0\;\;\;\;\; \\
r_0 &=& -(Ax_0 -b)\\
s_0 &=& -r_0 \\
r2 &=& <r_0,r_0> \\
\mbox{\textbf{for}}\;\; i=0, 1, \ldots \\
p_i &=& A s_i \\
\lambda_i &=& \frac{r2_i}{<s_i,p_i>} \\
x_{i+1} &=& x_{i} + \lambda_i s_i \\
r_{i+1} &=& r_i + \lambda_i p_i \\
r2_{new} &=& <r_{i+1},r_{i+1}> \\
s_{i+1} &=& -r_{i+1} + \frac{r2_{new}}{r2} s_i \\
r2_{i+1} &=& r2_{new} \\
\mbox{\textbf{return }                 } \;\;\;\;\;\;\;\;\;\;\; \\
\end{eqnarray*}


\begin{figure}[htbp]
\begin{center}
%\input{disperions.pdf}
%\input{dispersions.png}
\centerline{\includegraphics[width=8cm, height=6cm]{cg.pdf}}
\caption{The Conjugate Gradient method.}
\label{fig:SD}
\end{center}
\end{figure}


So we have just one matrix multiplication, $p_i = A s_i$, the costlier operation, for each step. There are some other variants of this method with similar properties.



\begin{thebibliography}{1}

  \bibitem{Meyer} Ilse C.F. Ipsen and Carl D. Meyer, {\em The Idea Behind Krylov Methods} http://meyer.math.ncsu.edu/Meyer/PS\_Files/Krylov.pdf
  
  \bibitem{Arnold} Douglas N. Arnold, {\em Lecture notes on Numerical Analysis of Partial Differential Equations}, 2011.
  
  \bibitem{Gutknecht} Martin H. Gutknecht, {\em A Brief Introduction to Krylov Space Methods for Solving Linear Systems}.
  http://www.sam.math.ethz.ch/~mhg/pub/biksm.pdf
  
  \bibitem{Shewchuk} Jonathan Richard Shewchuk, {\em An Introduction to the Conjugate Gradient Method Without the Agonizing Pain}, 1994. 
  https://www.cs.cmu.edu/~quake-papers/painless-conjugate-gradient.pdf
  
  \bibitem{Runar} Runar Heggelien Refsnæs, {\em A brief introduction to the conjugate gradient method}, 2008.
  http://www.idi.ntnu.no/~elster/tdt24/tdt24-f09/cg.pdf
  
  \bibitem{Shen} {\em Conjugate gradient method}, https://www.math.psu.edu/shen\_w/524/CG\_lecture.pdf
 
  \end{thebibliography}



\end{document}  

For simplicity we shall start our guess solution at $x_0=0$ if one wishes to use any other point then we do a translation and solve a similar problem but with a different source, 
$\hat{b} := b - Ax_0 := r_0$.
