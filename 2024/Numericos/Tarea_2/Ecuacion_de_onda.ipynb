{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ecuación de Ondas utilizando diferencias finitas y el método de líneas\n",
    "\n",
    "Autores: Oscar Reula, Joaquín Pelle, Pablo Montes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Utilizaremos el paquete `DifferentialEquations.jl`. \n",
    "Este notebook contiene modificaciones de ejemplos de las siguientes dos páginas:\n",
    "\n",
    "https://tutorials.sciml.ai/html/introduction/03-optimizing_diffeq_code.html\n",
    "\n",
    "http://juliamatrices.github.io/BandedMatrices.jl/latest/#Creating-banded-matrices-1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Método de líneas\n",
    "Resolveremos ecuaciones hiperbólicas utilizando el *método de líneas* y aproximaciones de *diferencias finitas*. Esto significa que si partimos de un sistema escalar de la forma\n",
    "\n",
    "\\begin{equation}\n",
    "u_t = u_x\n",
    "\\end{equation}\n",
    "\n",
    "y lo aproximaremos en un comienzo por un operador de diferencias finitas $D_x$ en la dirección espacial, para obtener\n",
    "\n",
    "\\begin{equation}\n",
    "v_t = D_x\\; v.\n",
    "\\end{equation}\n",
    "\n",
    "Donde $v$ es una versión de $u$ discretizada *solo* en el espacio. Esto es, si tomamos una grilla de $N$ puntos, $v$ será un vector de $N$ elementos que depende contínuamente en el tiempo. \n",
    "\n",
    "En este punto, tenemos efectivamente un sistema de ecuaciones ordinarias de dimensión $N$, al cual podemos aproximar utilizando un integrador ODE adecuado. De esta manera terminamos con una discretización en espacio y en tiempo."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ecuación de Onda\n",
    "\n",
    "Resolveremos la ecuación de onda \n",
    "\\begin{equation}\n",
    "\\phi_{tt} = \\phi_{xx}\n",
    "\\end{equation}\n",
    "\n",
    "Para expresarla en forma estándar definiremos dos nuevas variables, $u := \\phi_x$ y $v := \\phi_t$. Este sistema entonces se convierte en\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "\\phi_t & = & v \\\\\n",
    "v_t & = & u_x \\\\\n",
    "u_t & = & v_x,\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "donde hemos usado que $u_t := \\phi_{xt} = \\phi_{tx} := v_x$, y $v_t :=\\phi_{tt} = \\phi_{xx} = u_x$.  \n",
    "\n",
    "Dado que la ecuación para $\\phi$ puede ser integrada una vez conocidos $(u,v)$, y que $\\phi$ no es necesario para resolver el resto del sistema, lo ignoraremos por el momento.\n",
    "\n",
    "### Diagonalización del sistema\n",
    "\n",
    "Si definimos las variables $u^{+} = u+v$ y $u^{-} = u-v$ podemos obtener un sistema diagonalizado, \n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "u^+_t & = & u^+_x \\\\\n",
    "u^-_t & = & -u^-_x,\n",
    "\\end{align}\n",
    "$$\n",
    "donde la solución son dos ondas independientes, $u^{+}$ hacia la izquierda y $u^{-}$ hacia la derecha:\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "u^{+}(x,t) = u^{+}_0(x+t)\\\\\n",
    "u^{-}(x,t) = u^{-}_0(x-t)\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "Por lo tanto, las soluciones del sistema original serán combinaciones lineales de estas dos soluciones, las cuales dependen del dato inicial."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Por ejemplo, si tomamos como dato inicial $\\phi_0(x) = e^{-x^{2}}$ y $\\phi_t(x, t=0) = 0$ con condiciones periódicas de contorno en el intervalo $(-4,4)$, la solución exacta será"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "using Plots\n",
    "N = 201\n",
    "dx = 8.0/(N-1)\n",
    "x = range(-4.0, 4.0, length = N) #valores espaciales\n",
    "tiempos = range(0, 8, length = 200)              #valores temporales\n",
    "\n",
    "function return_to_interval(xini::Number, xfin::Number, x)\n",
    "    #Esta función toma un valor x y lo retorna al intervalo [xini, xfin)\n",
    "    length = xfin - xini\n",
    "    offset = x - xini\n",
    "    factor = offset / length\n",
    "    x = xini + (factor - floor(factor))*length\n",
    "end\n",
    "\n",
    "function up_0(x)\n",
    "    #(u^+)_0\n",
    "    x = return_to_interval(-4,4,x)\n",
    "    return -2*x*exp(-(x)^2) \n",
    "end\n",
    "function um_0(x)\n",
    "    #(u^-)_0\n",
    "    x = return_to_interval(-4,4,x)\n",
    "    return -2*x*exp(-(x)^2)\n",
    "end\n",
    "\n",
    "anim = @animate for t in tiempos\n",
    "    up = up_0.(x.+t)\n",
    "    um = um_0.(x.-t)\n",
    "    u = 0.5*(up+um)\n",
    "    v = 0.5*(up-um)\n",
    "    ϕ = [dx*sum(u[1:i]) for i in 1:N]\n",
    "    ϕ = ϕ .- minimum(ϕ)\n",
    "    lfs = 10\n",
    "    p1 = plot(x, ϕ, label = \"\\$ \\\\phi \\$\", ylim = (-0.1, 1.1), xlim = (-4,4), \n",
    "        xlabel = \"\\$x\\$\",legendfontsize = lfs)\n",
    "    title!(p1, \"Sistema original\")\n",
    "    p2 = plot(x, u, label = \"\\$u\\$\",legendfontsize = lfs)\n",
    "    plot!(p2, x, v, label = \"\\$v\\$\", ylim = (-1,1), xlim = (-4,4),\n",
    "        xlabel = \"\\$x\\$\", legendfontsize = lfs)\n",
    "    title!(p2, \"Nuevo Sistema\")\n",
    "    p3 = plot(x, up, label = \"\\$u^{+}\\$\", legendfontsize = lfs)\n",
    "    plot!(p3, x, um, label = \"\\$u^{-}\\$\", ylim = (-1,1), xlim = (-4,4),\n",
    "        xlabel = \"\\$x\\$\", legendfontsize = lfs)\n",
    "    title!(p3, \"Nuevo Sistema Diagonalizado\")\n",
    "    plot(p1, p2, p3, layout = (3,1), size=(400,600))\n",
    "end\n",
    "gif(anim, \"Ejemplo_Onda.gif\", fps = 30)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Estamos usando Julia, por lo que cargaremos algunos paquetes para manejar matrices, resolver ODEs y graficar."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "using OrdinaryDiffEq  #Esta es solo una parte del paquete DifferentialEquations\n",
    "#using DifferentialEquations\n",
    "using Plots\n",
    "using LinearAlgebra\n",
    "#import Pkg; Pkg.add(\"BandedMatrices\")\n",
    "using BandedMatrices\n",
    "using SparseArrays"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ahora agregamos algunos parámetros para la simulación. Algunos valores son abritrarios, y puede probar jugar con ellos. $N$ es el número de puntos en la discretización espacial. Resolveremos el problema *periódico*, por lo que si la grilla comienza en $1$ y termina en $N$, identificaremos los puntos $(N+1, N+2,...)$ con $(1, 2, ...)$ y los puntos $(0, -1, ...)$ con $(N, N-1, ...)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 200            # Numeros de puntos en la discretización espacial\n",
    "L = 1.0            # Intervalo espacial\n",
    "dx = L/N           # dx\n",
    "T = 10.            # Tiempo final\n",
    "dt = 1. *dx        # Tomamos dt ≈ dx/speed_max, para mantener la condición CFL y\n",
    "                   # Garantizar la estabilidad del algoritmo.\n",
    "\n",
    "r0=zeros(N,2)      # Discretización de los campos u y v\n",
    "x = [dx*i for i in 0:N-1];      # Coordenadas x, necesarias para determinar el dato inicial."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ahora definimos los esquemas de diferencias finitas. Están implementados como matrices que multiplican los vectores de la solución. Las matrices están definidas como dispersas para mayor eficiencia computacional.\n",
    "En general no es eficiente definir las derivadas numéricas con matrices, pero los casos con los que trabajaremos son bastante sencillos e ilustrativos."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function create_D_2_per(N)\n",
    "    D_2_per = sparse(Tridiagonal([-0.5 for i in 1:N-1],[0.0 for i in 1:N],[0.5 for i in 1:N-1]))\n",
    "    D_2_per[1,end] = -0.5\n",
    "    D_2_per[end,1] = 0.5\n",
    "    dropzeros!(D_2_per)\n",
    "    return D_2_per\n",
    "end\n",
    "\n",
    "\n",
    "function create_D2_2_per(N)\n",
    "    D2_2_per = BandedMatrix{Float64}(Zeros(N,N), (N-1,N-1))\n",
    "    D2_2_per[band(0)] .= -2.0\n",
    "    D2_2_per[band(1)] .= 1.0\n",
    "    D2_2_per[band(-1)] .= 1.0\n",
    "    \n",
    "    D2_2_per[band(N-1)] .= 1.0\n",
    "    D2_2_per[band(-N+1)] .= 1.0\n",
    "    \n",
    "    D2_2_per = sparse(D2_2_per)\n",
    "    dropzeros!(D2_2_per)\n",
    "    return D2_2_per\n",
    "end\n",
    "\n",
    "function create_D_4_per(N)\n",
    "    D_4_per = BandedMatrix{Float64}(Zeros(N,N), (N-1,N-1))\n",
    "    D_4_per[band(0)] .= 0.0\n",
    "    D_4_per[band(1)] .= 2.0/3.0\n",
    "    D_4_per[band(-1)] .= -2.0/3.0\n",
    "    D_4_per[band(2)] .= -1.0/12.0\n",
    "    D_4_per[band(-2)] .= 1.0/12.0\n",
    "    \n",
    "    D_4_per[band(N-1)] .= -2.0/3.0\n",
    "    D_4_per[band(N-2)] .= 1.0/12.0\n",
    "    \n",
    "    D_4_per[band(-N+1)] .= 2.0/3.0\n",
    "    D_4_per[band(-N+2)] .= -1.0/12.0\n",
    "    \n",
    "    D_4_per = sparse(D_4_per)\n",
    "    dropzeros!(D_4_per)\n",
    "    return D_4_per\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "println(\"Aproximación de segundo orden periódica de la derivada primera:\")\n",
    "println(create_D_2_per(8))\n",
    "println(\"Aproximación de cuarto orden periódica de la derivada primera:\")\n",
    "println(round.(create_D_4_per(8), digits = 4))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "round.(create_D_4_per(8), digits = 4)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Definimos ahora el lado derecho de las ecuaciones en el método de líneas, es decir, la discretización espacial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function F!(dr,r,p,t)\n",
    "    # second order version\n",
    "    dx, D = p\n",
    "    h = 1. /dx\n",
    "    u = @view r[:,1]\n",
    "    v = @view r[:,2]\n",
    "    du = @view dr[:,1]\n",
    "    dv = @view dr[:,2]\n",
    "    mul!(du, D, v, h, 0)  #du/dt = h*Dv\n",
    "    mul!(dv, D, u, h, 0)  #dv/dt = h*Du\n",
    "    #Nota: mul!(C, A, B, α, β) hace la operación α*A*B + β*C y la guarda en C\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ahora damos el dato inicial. En este caso particular, daremos uno en el que $u^{+}$ se anula. Por lo tanto, la única onda que debieramos ver es $u^{-}$ yendo hacia la derecha. Puede jugar y cambiar el dato inicial por otro."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x0 = 0.4; x1 = 0.6\n",
    "u = @view r0[:,1]\n",
    "v = @view r0[:,2]\n",
    "\n",
    "for i in 1:N\n",
    "    x[i] = dx*(i-1)\n",
    "    if x[i] > x0 && x[i] < x1\n",
    "        u[i] = (x[i] - x0)^8 * (x[i] - x1)^8 / (0.5*(x1-x0))^16\n",
    "        v[i] = -u[i]\n",
    "    end\n",
    "end\n",
    "\n",
    "plot(x, r0, label = [\"\\$u\\$\" \"\\$v\\$\"], xlabel = \"\\$x\\$\", legendfontsize = 15)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Definimos dos problemas con distinta precisión"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "D_2_per = create_D_2_per(N)\n",
    "D_4_per = create_D_4_per(N)\n",
    "\n",
    "p2 = (dx, D_2_per)\n",
    "p4 = (dx, D_4_per)\n",
    "@time prob2 = ODEProblem(F!,r0,(0.0,T),p2);\n",
    "@time prob4 = ODEProblem(F!,r0,(0.0,T),p4);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ahora resolvemos:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@time sol2 = solve(prob2,RK4(),dt=dt);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@time sol4 = solve(prob4,RK4(),dt=dt,adaptive = false);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finalmente, graficamos la solución a distintos tiempos. Notemos que, como la velocidad de propagación es $1$ y el sistema es periódico y de longitud espacial $1$, esperamos que para $t = i*1.0$ recuperemos el dato inicial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt = plot()\n",
    "for i in 0:4\n",
    "    plot!(plt,x, sol2(T*0.1*i)[:,1], label = \"t = $(T*0.1*i)\")\n",
    "end\n",
    "title!(\"Derivada de segundo orden\")\n",
    "xlabel!(\"\\$x\\$\")\n",
    "ylabel!(\"\\$u\\$\")\n",
    "display(plt)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt = plot()\n",
    "for i in 0:4\n",
    "    plot!(plt,x, sol4(T*0.2*i)[:,1], label = \"t = $(T*0.2*i)\")\n",
    "end\n",
    "title!(\"Derivada de cuarto orden\")\n",
    "xlabel!(\"\\$x\\$\")\n",
    "ylabel!(\"\\$u\\$\")\n",
    "display(plt)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "anim = @animate for i in 0:100\n",
    "    plot(x, sol2(i*T/100)[:,1], ylim = (-0.5, 1.5), label = \"t = $(i*T/100)\")\n",
    "end\n",
    "\n",
    "gif(anim, \"wave_anim_fps10.gif\", fps = 10)\n",
    "    "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.9.0",
   "language": "julia",
   "name": "julia-1.9"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.9.0"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
